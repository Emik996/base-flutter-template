import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/data/models/{{model_name.snakeCase()}}_model.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/domain/usecases/{{initial_use_case_name.snakeCase()}}_use_case.dart';

@override
  Future<BaseResponseEntity<{{model_name.pascalCase()}}Model>> {{initial_use_case_name.camelCase()}}({
    required {{initial_use_case_name.pascalCase()}}Params? params,
  }) async {
    return await _client.call<{{model_name.pascalCase()}}Model>(
      RestMethod.post,
      routes.{{api_route.camelCase()}},
      data: params?.toJson(),
      jsonToModel: {{model_name.pascalCase()}}Model.fromMap,
    );
  }
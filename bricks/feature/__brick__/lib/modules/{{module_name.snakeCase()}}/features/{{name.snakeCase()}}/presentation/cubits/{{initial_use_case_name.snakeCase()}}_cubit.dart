import 'package:{{project_name.snakeCase()}}/core/domain/base_cubit_state.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/domain/entities/{{model_name.snakeCase()}}_entity.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/domain/usecases/{{initial_use_case_name.snakeCase()}}_use_case.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

@injectable
class {{initial_use_case_name.pascalCase()}}Cubit extends Cubit<SimpleBaseBlocState<{{model_name.pascalCase()}}Entity>> {
  {{initial_use_case_name.pascalCase()}}Cubit({required {{initial_use_case_name.pascalCase()}}UseCase {{initial_use_case_name.camelCase()}}UseCase})
      : _{{initial_use_case_name.camelCase()}}UseCase = {{initial_use_case_name.camelCase()}}UseCase,
        super(const SimpleBaseBlocState.initial());

  final {{initial_use_case_name.pascalCase()}}UseCase _{{initial_use_case_name.camelCase()}}UseCase;

  Future<void> {{initial_use_case_name.camelCase()}}({
    {{initial_use_case_name.pascalCase()}}Params? params,
  }) async {
    emit(const SimpleBaseBlocState.loading());

    final result = await _{{initial_use_case_name.camelCase()}}UseCase.call(params);

    result.fold((model) {
      emit(SimpleBaseBlocState.loaded(model));
    }, (error) {
      emit(SimpleBaseBlocState.error(error));
    });
  }
}

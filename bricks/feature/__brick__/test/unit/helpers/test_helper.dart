import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/domain/usecases/{{initial_use_case_name.snakeCase()}}_use_case.dart';

{{initial_use_case_name.pascalCase()}}UseCase,
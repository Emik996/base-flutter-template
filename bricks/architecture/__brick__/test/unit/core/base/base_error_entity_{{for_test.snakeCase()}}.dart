import 'package:{{project_name.snakeCase()}}/core/domain/base_error_entity.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test(
    'ServerErrorEntity',
    () {
      final error = ServerErrorEntity(
        response: Response(
          statusCode: 400,
          data: {
            'error': 'Error has occurred!',
            'detail': 'Try again later!',
          },
          requestOptions: RequestOptions(),
        ),
      );

      expect(error.exception.title, 'Error has occurred!');
      expect(error.exception.message, 'Try again later!');
    },
  );
  test(
    'DioErrorEntity',
    () {
      final error = DioErrorEntity(
        response: Response(
          statusCode: 400,
          data: {
            'error': 'Error has occurred!',
            'detail': 'Try again later!',
          },
          requestOptions: RequestOptions(),
        ),
      );

      expect(error.exception.title, 'Error has occurred!');
      expect(error.exception.message, 'Try again later!');
    },
  );
  test(
    'CustomErrorEntity',
    () {
      final error = CustomErrorEntity(
        title: 'Error has occurred!',
        message: 'Try again later!',
      );

      expect(error.exception.title, 'Error has occurred!');
      expect(error.exception.message, 'Try again later!');
    },
  );
  test(
    'NoConnectionErrorEntity',
    () {
      final error = NoConnectionErrorEntity();

      expect(error.exception.title, 'Check your connection');
      expect(error.exception.message, 'No internet connection');
      expect(error.exception.statusCode, -3);
    },
  );
}

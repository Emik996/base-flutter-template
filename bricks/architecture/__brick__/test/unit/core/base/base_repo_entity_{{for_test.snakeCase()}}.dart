import 'package:{{project_name.snakeCase()}}/core/domain/base_error_entity.dart';
import 'package:{{project_name.snakeCase()}}/core/domain/base_repo_entity.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test(
    'BaseRepoEntity has an error, but data model is null',
    () {
      final repoEntity = BaseResponseEntity(
          error: DioErrorEntity(
        response: Response(
          statusCode: 400,
          data: {
            'error': 'Error has occurred!',
            'detail': 'Try again later!',
          },
          requestOptions: RequestOptions(),
        ),
      ));

      expect(repoEntity.model, null);
      expect(repoEntity.error!.exception.title, 'Error has occurred!');
      expect(repoEntity.error!.exception.message, 'Try again later!');
      expect(repoEntity.error!.exception.statusCode, 400);
    },
  );
  test(
    'BaseRepoEntity has a model as string type, but error is null',
    () {
      final repoEntity = BaseResponseEntity(
        model: 'some model',
      );

      expect(repoEntity.error, null);
      expect(repoEntity.model.runtimeType, String);
      expect(repoEntity.model, 'some model');
    },
  );
  test(
    'BaseRepoEntity has a model as double type, but error is null',
    () {
      final repoEntity = BaseResponseEntity(
        model: 100.0,
      );

      expect(repoEntity.error, null);
      expect(repoEntity.model.runtimeType, double);
      expect(repoEntity.model, 100.0);
    },
  );
}

import 'dart:io';

String fixtureReader(String name) =>
    File('test/unit/fixtures/$name').readAsStringSync();

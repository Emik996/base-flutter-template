import 'package:{{project_name.snakeCase()}}/core/data/network/network_info.dart';
import 'package:{{project_name.snakeCase()}}/core/data/network/rest_client.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:mockito/annotations.dart';
import 'package:shared_preferences/shared_preferences.dart';

@GenerateMocks(
  [
    /// Data / Data Sources
    Dio,
    NetworkInfo,
    RestClient,
    SharedPreferences,
    Connectivity,

    /// Repositories / Use cases
  ],
)
void main() {}

import 'package:{{project_name.snakeCase()}}/core/config/di/injection.dart';
import 'package:{{project_name.snakeCase()}}/core/config/l10n/locale_provider.dart';
import 'package:{{project_name.snakeCase()}}/core/config/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InitWidget extends StatelessWidget {
  const InitWidget({
    Key? key,
    required this.builder,
  }) : super(key: key);

  final Widget Function(BuildContext, Widget?) builder;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => LocaleProvider(
            prefs: di<SharedPreferences>(),
          ),
        ),
        ChangeNotifierProvider(
          create: (context) => AppThemeProvider(
            prefs: di<SharedPreferences>(),
          )..init(),
        ),
      ],
      child: ScreenUtilInit(
        designSize: const Size(360.0, 640.0),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: builder,
      ),
    );
  }
}

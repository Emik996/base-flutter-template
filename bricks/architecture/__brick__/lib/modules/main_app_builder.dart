import 'package:{{project_name.snakeCase()}}/core/config/di/injection.dart';
import 'package:{{project_name.snakeCase()}}/core/config/l10n/generated/l10n.dart';
import 'package:{{project_name.snakeCase()}}/core/config/router/router.dart';
import 'package:{{project_name.snakeCase()}}/core/domain/base_app_builder.dart';
import 'package:{{project_name.snakeCase()}}/core/domain/base_cubit_state.dart';
import 'package:{{project_name.snakeCase()}}/core/extensions/context_extension.dart';
import 'package:{{project_name.snakeCase()}}/core/presentation/cubits/connection_cubit.dart';
import 'package:{{project_name.snakeCase()}}/modules/init_widget.dart';
import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

final scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();
final appRouter = AppRouter();

class MainAppBuilder implements BaseAppBuilder {
  @override
  Widget buildApp() {
    return InitWidget(
      builder: (context, child) {
        return MaterialApp.router(
          title: 'Flutter App',
          scaffoldMessengerKey: scaffoldMessengerKey,
          debugShowCheckedModeBanner: false,
          routerDelegate: appRouter.delegate(),
          routeInformationParser: appRouter.defaultRouteParser(),
          routeInformationProvider: appRouter.routeInfoProvider(),
          supportedLocales: L10n.delegate.supportedLocales,
          localizationsDelegates: context.localeProvider.l10nDelegates,
          locale: context.locale.locale,
          theme: context.themeProvider.lightTheme,
          darkTheme: context.themeProvider.darkTheme,
          themeMode: context.theme.themeMode,
          builder: _materialbuilder,
        );
      },
    );
  }

  Widget _materialbuilder(BuildContext context, Widget? child) {
    return BlocProvider.value(
      value: di<ConnectionCubit>()..listen(),
      child: BlocListener<ConnectionCubit, SimpleBaseBlocState>(
        listener: (context, state) {
          if (state.status == StateStatus.error) {
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
              content: Text('Нет интернет соединения!'),
              backgroundColor: Colors.yellow,
              duration: Duration(days: 30),
              behavior: SnackBarBehavior.floating,
            ));
          }
          if (state.status == StateStatus.initial) {
            ScaffoldMessenger.of(context).clearSnackBars();
          }
        },
        child: DevicePreview.appBuilder(context, child),
      ),
    );
  }
}

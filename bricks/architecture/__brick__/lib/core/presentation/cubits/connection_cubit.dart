import 'package:{{project_name.snakeCase()}}/core/data/network/network_info.dart';
import 'package:{{project_name.snakeCase()}}/core/domain/base_cubit_state.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

@injectable
class ConnectionCubit extends Cubit<SimpleBaseBlocState> {
  ConnectionCubit({required NetworkInfo networkInfo})
      : _networkInfo = networkInfo,
        super(const SimpleBaseBlocState.loading());

  final NetworkInfo _networkInfo;

  Future<void> listen() async {
    final isConnected = await _networkInfo.isConnected;

    await Future.delayed(const Duration(milliseconds: 800), () {
      if (isConnected) {
        emit(const SimpleBaseBlocState.initial());
      } else {
        emit(const SimpleBaseBlocState.error(null));
      }
    });

    await _listenForStatus();
  }

  Future<void> _listenForStatus() async {
    _networkInfo.service.onConnectivityChanged.listen((status) {
      if (status == ConnectivityResult.none) {
        emit(const SimpleBaseBlocState.error(null));
      } else {
        emit(const SimpleBaseBlocState.initial());
      }
    });
  }
}

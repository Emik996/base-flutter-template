import 'package:equatable/equatable.dart';
import 'base_error_entity.dart';

enum StateStatus { loaded, initial, loading, error, added, edited, deleted }

final class SimpleBaseBlocState<T> extends Equatable {
  final T? element;
  final dynamic subContainer;
  final BaseErrorEntity? error;
  final StateStatus status;
  const SimpleBaseBlocState({
    this.element,
    required this.subContainer,
    this.error,
    required this.status,
  });

  const SimpleBaseBlocState._({
    this.element,
    this.error,
    this.status = StateStatus.initial,
    this.subContainer,
  });

  const SimpleBaseBlocState.loaded(T? element)
      : this._(
          element: element,
          status: StateStatus.loaded,
        );
  const SimpleBaseBlocState.initial({T? element})
      : this._(
          status: StateStatus.initial,
          element: element,
        );
  const SimpleBaseBlocState.loading({T? element})
      : this._(
          element: element,
          status: StateStatus.loading,
        );
  const SimpleBaseBlocState.error(BaseErrorEntity? error, {T? element})
      : this._(
          error: error,
          status: StateStatus.error,
          element: element,
        );
  const SimpleBaseBlocState.added(T? element, {dynamic subContainer})
      : this._(
          element: element,
          status: StateStatus.added,
          subContainer: subContainer,
        );
  const SimpleBaseBlocState.edited(T? element)
      : this._(
          element: element,
          status: StateStatus.edited,
        );
  const SimpleBaseBlocState.deleted(T? element, int id)
      : this._(
          element: element,
          subContainer: id,
          status: StateStatus.deleted,
        );

  @override
  List<Object?> get props => [status];
}

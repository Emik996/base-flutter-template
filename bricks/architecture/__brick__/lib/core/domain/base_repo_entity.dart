import 'base_error_entity.dart';

class BaseResponseEntity<E> {
  BaseResponseEntity({
    BaseErrorEntity? error,
    E? model,
  })  : _error = error,
        _model = model;

  final BaseErrorEntity? _error;
  final E? _model;

  bool get isSuccess => _error == null;
  E? get model => _model;
  BaseErrorEntity? get error => _error;

  fold(
    Function(E model) onResult,
    Function(BaseErrorEntity error) onError,
  ) {
    if (isSuccess) {
      return onResult(_model as E);
    } else {
      return onError(_error ?? NoConnectionErrorEntity());
    }
  }

  @override
  String toString() => 'BaseRepoEntity(_error: $_error, _model: $_model)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is BaseResponseEntity<E> &&
        other._error == _error &&
        other._model == _model;
  }

  @override
  int get hashCode => _error.hashCode ^ _model.hashCode;

  BaseResponseEntity<E> copyWith({
    BaseErrorEntity? error,
    E? model,
  }) {
    return BaseResponseEntity<E>(
      error: error ?? _error,
      model: model ?? _model,
    );
  }
}

import 'package:dio/dio.dart';

abstract base class BaseErrorEntity {
  BaseNetworkExceptions get exception;
}

final class ServerErrorEntity extends BaseErrorEntity {
  final Response? response;
  ServerErrorEntity({this.response});

  @override
  BaseNetworkExceptions get exception => BaseNetworkExceptions(
        statusCode: response?.statusCode,
        response: response,
      );
}

final class DioErrorEntity extends BaseErrorEntity {
  final Response? response;
  DioErrorEntity({this.response});

  @override
  BaseNetworkExceptions get exception => BaseNetworkExceptions(
        statusCode: response?.statusCode,
        response: response,
      );
}

final class CustomErrorEntity extends BaseErrorEntity {
  final String title;
  final int? statusCode;
  String message;

  CustomErrorEntity({
    required this.title,
    this.message = '',
    this.statusCode = -3,
  }) {
    if (message.isEmpty) {
      message = title;
    }
  }

  @override
  BaseNetworkExceptions get exception => BaseNetworkExceptions(
        statusCode: statusCode,
        title: title,
        message: message,
      );
}

final class NoConnectionErrorEntity extends BaseErrorEntity {
  @override
  BaseNetworkExceptions get exception => BaseNetworkExceptions(statusCode: -3);
}

final class BaseNetworkExceptions {
  String title;
  String message;
  final int? statusCode;
  final Response? response;

  BaseNetworkExceptions({
    this.title = '',
    this.message = '',
    this.statusCode = -3,
    this.response,
  }) {
    if (title.isEmpty && message.isEmpty) {
      switch (statusCode) {
        case -3:
          title = 'Check your connection';
          message = 'No internet connection';
          break;
        case 500:
          title = 'Server Error';
          message = 'Internal Server Error';
          break;
        default:
          title = _parseErrorTitle(response);
          message = _parseErrorSubtitle(response);
          break;
      }
    }
  }

  @override
  String toString() {
    return 'BaseNetworkExceptions(title: $title, message: $message, statusCode: $statusCode, response: $response)';
  }

  String _parseErrorTitle(Response? response) {
    String localError = '';
    try {
      localError = response?.data['error'];
    } catch (e) {
      localError = 'No internet connection';
    }
    return localError;
  }

  String _parseErrorSubtitle(Response? response) {
    String localError = '';
    try {
      localError = response?.data['detail'];
    } catch (e) {
      localError = 'No internet connection';
    }
    return localError;
  }
}

class StorageKeys {
  static const String token = 'APP_TOKEN';
  static const String refreshToken = 'APP_REFRESH_TOKEN';
  static const String rnm = 'APP_RNM';
  static const String locale = 'APP_LOCALE';
  static const String themeMode = 'APP_THEME_MODE';
  static const String pincode = 'APP_PINCODE';
  static const String isPinLocked = 'APP_IS_PINCODE_LOCKED';
  static const String isBiometric = 'APP_IS_BIOMETRIC';
  static const String selectedPrinter = 'APP_SELECTED_PRINTER';
}

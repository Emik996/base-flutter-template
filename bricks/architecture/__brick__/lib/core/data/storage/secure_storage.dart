import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class SecureStorage {
  final FlutterSecureStorage _storage;

  SecureStorage({required FlutterSecureStorage storage}) : _storage = storage;

  Future<void> save({
    required String key,
    required String value,
  }) async {
    await _storage.write(key: key, value: value);
  }

  Future<String> read({required String key}) async {
    return await _storage.read(key: key) ?? '';
  }
}

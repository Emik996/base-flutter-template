import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:injectable/injectable.dart';

abstract class NetworkInfo {
  Future<bool> get isConnected;
  Connectivity get service;
}

@LazySingleton(as: NetworkInfo)
final class NetworkInfoImpl implements NetworkInfo {
  NetworkInfoImpl({
    required Connectivity connectivity,
  }) : _connectivity = connectivity;

  late final Connectivity _connectivity;

  @override
  Future<bool> get isConnected async {
    ConnectivityResult result = await _connectivity.checkConnectivity();
    return result != ConnectivityResult.none;
  }

  @override
  Connectivity get service => _connectivity;
}

import 'dart:developer';
import 'package:{{project_name.snakeCase()}}/core/data/storage/storage_keys.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@lazySingleton
final class BaseInterceptors implements Interceptor {
  BaseInterceptors({required SharedPreferences prefs}) : _prefs = prefs;
  final SharedPreferences _prefs;

  @override
  void onError(DioException error, ErrorInterceptorHandler handler) {
    // if (error.response == null) {
    // } else if (error.response?.statusCode == 401) {
    //   try {
    //     final savedToken = _prefs.getString(Constants.token) ?? '';

    //     if (savedToken != '') {
    //       final tokenJson = json.decode(savedToken);
    //       final oldToken = const TokenEntity().fromJson(tokenJson);
    //       final refreshTime =
    //           DateTime.tryParse(oldToken.refreshTokenTtl!) ??
    //               DateTime.now();

    //       if (refreshTime.isAfter(DateTime.now())) {
    //         final refreshRequest = await _refreshToken(oldToken: oldToken);
    //         final token = refreshRequest.model?.toJson();
    //         final stringToken = jsonEncode(token);
    //         await _prefs.setString(Constants.token, stringToken);

    //         error.requestOptions.headers.addAll({
    //           'Access-Token': refreshRequest.model?.accessToken,
    //         });

    //         final response = await Dio().post(
    //           error.requestOptions.uri.toString(),
    //           options: Options(headers: error.requestOptions.headers),
    //         );

    //         return handler.resolve(response);
    //       } else {
    //         return handler.next(error);
    //       }
    //     }
    //   } on DioError catch (error) {
    return handler.next(error);
    // }
    // }

    // if (error.dioErrorStatusCode != 418 &&
    //     error.requestOptions.path !=
    //         'https://app1.megacom.kg:9090/ofd-mobile-test/api/v1/cashReg/set/token') {
    //   AppToasts.showErrorSnackBar(error.dioErrorMessage);
    // }

    // return handler.next(error);
  }

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    final savedToken = _prefs.getString(StorageKeys.token) ?? '';
    final savedRNM = _prefs.getString(StorageKeys.rnm) ?? '';
    if (savedToken != '' && savedRNM != '') {
      options.headers.addAll({
        'Access-Token': savedToken,
        'rnm': savedRNM,
      });
    }

    options.headers.addEntries([
      const MapEntry('lang', 'ru'),
    ]);

    log(options.headers.toString());

    return handler.next(options);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    return handler.next(response);
  }

  // Future<AuthRepoResult> _refreshToken({required TokenEntity oldToken}) async {
  //   try {
  //     final result = await Dio().post(
  //       '${AppFlavors.baseUrl}${ApiRoutes.authRefresh}',
  //       options: Options(
  //         headers: {
  //           'Access-Token': oldToken.accessToken,
  //           'Refresh-Token': oldToken.refreshToken,
  //         },
  //       ),
  //     );

  //     return AuthRepoResult(
  //       model: const TokenEntity().fromJson(result.data),
  //     );
  //   } catch (error) {
  //     return AuthRepoResult(
  //       error: AppErrorEntity(
  //         message: error.dioErrorMessage,
  //         code: error.dioErrorStatusCode,
  //       ),
  //     );
  //   }
  // }
}

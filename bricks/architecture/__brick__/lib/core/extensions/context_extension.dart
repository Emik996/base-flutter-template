import 'package:{{project_name.snakeCase()}}/core/config/l10n/generated/l10n.dart';
import 'package:{{project_name.snakeCase()}}/core/config/l10n/locale_provider.dart';
import 'package:{{project_name.snakeCase()}}/core/config/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

extension ContextExtension on BuildContext {
  L10n get lang => L10n.of(this);
  LocaleProvider get locale => watch<LocaleProvider>();
  LocaleProvider get localeProvider => read<LocaleProvider>();
  AppThemeProvider get theme => watch<AppThemeProvider>();
  AppThemeProvider get themeProvider => read<AppThemeProvider>();
  double get width => MediaQuery.of(this).size.width;
  double get height => MediaQuery.of(this).size.height;
}

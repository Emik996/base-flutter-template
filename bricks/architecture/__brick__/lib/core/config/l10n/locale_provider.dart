import 'dart:io';
import 'package:{{project_name.snakeCase()}}/core/config/l10n/generated/l10n.dart';
import 'package:{{project_name.snakeCase()}}/core/data/storage/storage_keys.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocaleProvider extends ChangeNotifier {
  LocaleProvider({required SharedPreferences prefs}) {
    _prefs = prefs;
    _initialize();
  }

  late final SharedPreferences _prefs;
  late Locale _locale;
  Locale get locale => _locale;
  List<Locale> get locales => const [
        Locale('ru'),
        Locale('ky'),
      ];

  void _initialize() {
    final lc = _prefs.getString(StorageKeys.locale);
    if (lc == null) {
      final deviceLocale = Platform.localeName.split('_').first;

      if (deviceLocale == 'ky' || deviceLocale == 'ru') {
        _locale = Locale(deviceLocale);
      } else {
        _locale = const Locale('ky');
      }
    } else {
      _locale = Locale(lc);
    }

    notifyListeners();
  }

  void setLocale(_) {
    if (_locale == locales.first) {
      _locale = locales.last;
    } else {
      _locale = locales.first;
    }

    _prefs.setString(StorageKeys.locale, _locale.toString());

    notifyListeners();
  }

  final l10nDelegates = const <LocalizationsDelegate>[
    L10n.delegate,
    GlobalWidgetsLocalizations.delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
  ];
}

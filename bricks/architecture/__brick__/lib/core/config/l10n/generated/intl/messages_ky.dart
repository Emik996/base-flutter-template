// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ky locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ky';

  static String m0(kassaId) =>
      "\nОсОО Касса плюс #${kassaId}\nМагазин, 72000, с. Панфилова, ул";

  static String m1(fee) => "Абонплата 1 мес - ${fee} сом (Мобильный)";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "anErrorHasOccured":
            MessageLookupByLibrary.simpleMessage("Произошла ошибка"),
        "balance": MessageLookupByLibrary.simpleMessage("Баланс"),
        "cancel": MessageLookupByLibrary.simpleMessage("Отменить"),
        "cantBeEmpty":
            MessageLookupByLibrary.simpleMessage("Поле не может быть пустым."),
        "cash": MessageLookupByLibrary.simpleMessage("Наличные"),
        "cashInDesc": MessageLookupByLibrary.simpleMessage("Наличные в кассе"),
        "cashbox": MessageLookupByLibrary.simpleMessage("Смена"),
        "cashboxAndReceipts":
            MessageLookupByLibrary.simpleMessage("Смена и чеки"),
        "cashlessPayment":
            MessageLookupByLibrary.simpleMessage("Безналичная оплата"),
        "checkYourBluetoothConnection": MessageLookupByLibrary.simpleMessage(
            "Проверьте ваше подключение к Bluetooth."),
        "checkYourConnection": MessageLookupByLibrary.simpleMessage(
            "Проверьте, пожалуйста, свое соединение с интернетом!"),
        "checkYourPrinterConnection":
            MessageLookupByLibrary.simpleMessage("Проверьте свое подключение"),
        "checkingNotAvailable":
            MessageLookupByLibrary.simpleMessage("Проверка не доступна"),
        "closeShift":
            MessageLookupByLibrary.simpleMessage("Закрыть рабочую\nсмену"),
        "colorsettings":
            MessageLookupByLibrary.simpleMessage("Настройки цвета"),
        "comeIn": MessageLookupByLibrary.simpleMessage("Войти"),
        "confirm": MessageLookupByLibrary.simpleMessage("Подтвердить"),
        "connectWithSupport":
            MessageLookupByLibrary.simpleMessage("Свяжитесь с поддержкой"),
        "connectionError":
            MessageLookupByLibrary.simpleMessage("Ошибка подключения"),
        "createPincode":
            MessageLookupByLibrary.simpleMessage("Создайте 4-значный пин-код"),
        "cretePincode":
            MessageLookupByLibrary.simpleMessage("Создайте код доступа"),
        "cto": MessageLookupByLibrary.simpleMessage("ЦТО"),
        "currentCashbox": MessageLookupByLibrary.simpleMessage("Текущая смена"),
        "darkTheme": MessageLookupByLibrary.simpleMessage("Темная тема"),
        "depositMoeny":
            MessageLookupByLibrary.simpleMessage("Внесение средств"),
        "deviceNotSupported": MessageLookupByLibrary.simpleMessage(
            "Устройство не поддерживается"),
        "doYouWantExit": MessageLookupByLibrary.simpleMessage(
            "Вы хотите выйти с приложения?"),
        "emptyPage": MessageLookupByLibrary.simpleMessage("Пустая страница."),
        "enterAmmount": MessageLookupByLibrary.simpleMessage("Введите сумму"),
        "enterPassword": MessageLookupByLibrary.simpleMessage("Введите пароль"),
        "enterPincode":
            MessageLookupByLibrary.simpleMessage("Введите код для доступа"),
        "exit": MessageLookupByLibrary.simpleMessage("Выход"),
        "failedToLoadTryAgain": MessageLookupByLibrary.simpleMessage(
            "Не удалось загрузить. Повторите еще раз"),
        "failedToMakePayment":
            MessageLookupByLibrary.simpleMessage("Не удалось провести оплату"),
        "fingerprint":
            MessageLookupByLibrary.simpleMessage("Отпечаток пальцев"),
        "forgotPincode":
            MessageLookupByLibrary.simpleMessage("Забыли пин-код?"),
        "gnsNotAvailable": MessageLookupByLibrary.simpleMessage(
            "На данный момент ГНС недоступен"),
        "help": MessageLookupByLibrary.simpleMessage("Помощь"),
        "incorrectData":
            MessageLookupByLibrary.simpleMessage("Некорректные данные."),
        "incorrectPassword":
            MessageLookupByLibrary.simpleMessage("Пароль введен неверно"),
        "input": MessageLookupByLibrary.simpleMessage("Вход"),
        "kassaInfo": m0,
        "languageSelection": MessageLookupByLibrary.simpleMessage("qwdqwdqw"),
        "makeCall": MessageLookupByLibrary.simpleMessage("Позвонить"),
        "makePayment": MessageLookupByLibrary.simpleMessage("Оплатить"),
        "mustContains16Symbols": MessageLookupByLibrary.simpleMessage(
            "Поле должно содержать 16 символов."),
        "no": MessageLookupByLibrary.simpleMessage("Нет"),
        "noData": MessageLookupByLibrary.simpleMessage("Нет данных"),
        "noInternet":
            MessageLookupByLibrary.simpleMessage("Нет интернет соединения"),
        "nonCash": MessageLookupByLibrary.simpleMessage("Безналичные"),
        "odd": MessageLookupByLibrary.simpleMessage("Сдача"),
        "openShift":
            MessageLookupByLibrary.simpleMessage("Открыть рабочую\nсмену"),
        "opens": MessageLookupByLibrary.simpleMessage("Открыта"),
        "osNotSupported":
            MessageLookupByLibrary.simpleMessage("Не поддерживаемая ОС"),
        "passBioForEnter": MessageLookupByLibrary.simpleMessage(
            "Пройдите биометрию для входа в приложение"),
        "password": MessageLookupByLibrary.simpleMessage("Пароль"),
        "phone": MessageLookupByLibrary.simpleMessage("т."),
        "pinNotInstalled":
            MessageLookupByLibrary.simpleMessage("Не установлен PIN-code"),
        "pincodesNotEqual":
            MessageLookupByLibrary.simpleMessage("Пин-коды не совпадают"),
        "print": MessageLookupByLibrary.simpleMessage("Печать"),
        "print2": MessageLookupByLibrary.simpleMessage("Распечатать"),
        "printer": MessageLookupByLibrary.simpleMessage("qwdqwd"),
        "printerConnection":
            MessageLookupByLibrary.simpleMessage("Подключение принтера"),
        "printersNotFound":
            MessageLookupByLibrary.simpleMessage("Принтеры не найдены."),
        "printetNotFound":
            MessageLookupByLibrary.simpleMessage("Принтер не обнаружен"),
        "proceeds": MessageLookupByLibrary.simpleMessage("Выручка"),
        "quantity": MessageLookupByLibrary.simpleMessage("Количество"),
        "receipt": MessageLookupByLibrary.simpleMessage("Чек"),
        "receiptReturn": MessageLookupByLibrary.simpleMessage("Возврат чека"),
        "receipts": MessageLookupByLibrary.simpleMessage("Чеки"),
        "received": MessageLookupByLibrary.simpleMessage("Принято"),
        "refreshPage":
            MessageLookupByLibrary.simpleMessage("Перезагрузите страницу!"),
        "releaseDate": MessageLookupByLibrary.simpleMessage("Дата выпуска"),
        "repeat": MessageLookupByLibrary.simpleMessage("Повторить"),
        "repeatPincode":
            MessageLookupByLibrary.simpleMessage("Повторите код доступа"),
        "reports": MessageLookupByLibrary.simpleMessage("Отчеты"),
        "returnQuantity":
            MessageLookupByLibrary.simpleMessage("Количество возвратов"),
        "returnSumm": MessageLookupByLibrary.simpleMessage("Сумма возвратов"),
        "rnm": MessageLookupByLibrary.simpleMessage("РНМ"),
        "safety": MessageLookupByLibrary.simpleMessage("xzczxczxc"),
        "saleReturn": MessageLookupByLibrary.simpleMessage("Возврат продажи"),
        "searchByReceiptNumber":
            MessageLookupByLibrary.simpleMessage("Поиск по номеру чека"),
        "searching": MessageLookupByLibrary.simpleMessage("Поиск"),
        "sell": MessageLookupByLibrary.simpleMessage("Продажа"),
        "sells": MessageLookupByLibrary.simpleMessage("Продажи"),
        "serverError": MessageLookupByLibrary.simpleMessage("Ошибка сервера."),
        "settings": MessageLookupByLibrary.simpleMessage("Настройки"),
        "share": MessageLookupByLibrary.simpleMessage("Поделиться"),
        "shouldClose":
            MessageLookupByLibrary.simpleMessage("Необходимо закрыть"),
        "shouldUseBio": MessageLookupByLibrary.simpleMessage(
            "Использовать биометрию для входа?"),
        "smthGoneWrong":
            MessageLookupByLibrary.simpleMessage("Что то пошло не так"),
        "state": MessageLookupByLibrary.simpleMessage("Состояние"),
        "subscriptionFee": m1,
        "summ": MessageLookupByLibrary.simpleMessage("Сумма"),
        "systemError":
            MessageLookupByLibrary.simpleMessage("Произошла системная ошибка"),
        "tariffActiveUntil":
            MessageLookupByLibrary.simpleMessage("Тариф активен до"),
        "timeLeft": MessageLookupByLibrary.simpleMessage("Осталось времени"),
        "timeOut":
            MessageLookupByLibrary.simpleMessage("Истекло время ожидания"),
        "tryAgain": MessageLookupByLibrary.simpleMessage("Попробуйте заново"),
        "tryAgainLater": MessageLookupByLibrary.simpleMessage(
            "Попробуйте, пожалуйста, через некоторое время!"),
        "unvalidPincode":
            MessageLookupByLibrary.simpleMessage("Неверный код доступа"),
        "version": MessageLookupByLibrary.simpleMessage("Версия"),
        "whiteTheme": MessageLookupByLibrary.simpleMessage("Светлая тема"),
        "withdrawMoney":
            MessageLookupByLibrary.simpleMessage("Изъятие средств"),
        "yes": MessageLookupByLibrary.simpleMessage("Да"),
        "yourConnectionIsGone": MessageLookupByLibrary.simpleMessage(
            "Кажется, ваше подключение истекло!")
      };
}

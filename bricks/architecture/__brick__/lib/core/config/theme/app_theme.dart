import 'package:{{project_name.snakeCase()}}/core/data/storage/storage_keys.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'app_colors.dart';
part 'app_decorations.dart';
part 'app_text_styles.dart';

class AppThemeProvider extends ChangeNotifier {
  AppThemeProvider({required SharedPreferences prefs}) {
    _prefs = prefs;
  }
  late final SharedPreferences _prefs;
  static ThemeMode _themeMode = ThemeMode.system;

  ThemeMode get themeMode => _themeMode;

  bool get isDark {
    final brightness = PlatformDispatcher.instance.platformBrightness;
    return brightness == Brightness.dark;
  }

  late bool isDarkThemeActive;

  static Brightness get brightness =>
      PlatformDispatcher.instance.platformBrightness;

  void init() {
    var theme = _prefs.getString(
          StorageKeys.themeMode,
        ) ??
        ThemeMode.system.name;
    _themeMode = ThemeMode.values.byName(theme);

    isDarkThemeActive = _themeMode == ThemeMode.dark;

    if (_themeMode == ThemeMode.system) {
      if (isDark) {
        _themeMode = ThemeMode.dark;
        isDarkThemeActive = true;
      } else {
        _themeMode = ThemeMode.light;
        isDarkThemeActive = false;
      }
    }

    notifyListeners();
  }

  void changeThemeMode() {
    switch (themeMode) {
      case ThemeMode.dark:
        _themeMode = ThemeMode.light;
        isDarkThemeActive = false;
        break;
      case ThemeMode.light:
        _themeMode = ThemeMode.dark;
        isDarkThemeActive = true;
        break;
      case ThemeMode.system:
        _themeMode =
            (brightness == Brightness.dark) ? ThemeMode.light : ThemeMode.dark;

        if (brightness == Brightness.dark) {
          isDarkThemeActive = true;
        } else {
          isDarkThemeActive = false;
        }
        break;
      default:
        _themeMode = ThemeMode.light;
        isDarkThemeActive = false;
    }
    _prefs.setString(StorageKeys.themeMode, themeMode.name);
    notifyListeners();
  }

  static AppColors get colors {
    switch (_themeMode) {
      case ThemeMode.light:
        return AppLightColors();

      case ThemeMode.dark:
        return AppDarkColors();

      case ThemeMode.system:
        return brightness == Brightness.dark
            ? AppLightColors()
            : AppDarkColors();
      default:
        return AppDarkColors();
    }
  }

  static AppDecorations get decors {
    switch (_themeMode) {
      case ThemeMode.light:
        return LightDecorations();

      case ThemeMode.dark:
        return DarkDecorations();

      case ThemeMode.system:
        return brightness == Brightness.dark
            ? LightDecorations()
            : DarkDecorations();
      default:
        return DarkDecorations();
    }
  }

  ThemeData get lightTheme => ThemeData.light().copyWith();

  ThemeData get darkTheme => ThemeData.dark().copyWith();
}

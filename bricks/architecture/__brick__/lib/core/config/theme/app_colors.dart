part of 'app_theme.dart';

abstract base class AppColors {
  abstract Color mainTextColor;
}

final class AppLightColors extends AppColors {
  @override
  Color mainTextColor = const Color(0xff000000);
}

final class AppDarkColors extends AppColors {
  @override
  Color mainTextColor = const Color(0xffFFFFFF);
}

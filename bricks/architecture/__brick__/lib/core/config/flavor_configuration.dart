import 'package:{{project_name.snakeCase()}}/core/data/network/api_routes.dart'
    as routes;

enum Flavor { dev, prod, stg }

abstract class AppFlavors {
  static Flavor? flavor;

  static String get baseUrl {
    switch (flavor) {
      case Flavor.prod:
        return routes.baseProd;
      case Flavor.dev:
        return routes.baseDev;
      default:
        return routes.baseStg;
    }
  }
}

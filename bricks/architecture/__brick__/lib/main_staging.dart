import 'package:{{project_name.snakeCase()}}/core/config/flavor_configuration.dart';
import 'package:{{project_name.snakeCase()}}/modules/main_app_builder.dart';
import 'package:{{project_name.snakeCase()}}/modules/main_app_runner.dart';

void main() {
  final runner = MainAppRunner();
  final builder = MainAppBuilder();
  runner.run(builder, Flavor.stg);
}

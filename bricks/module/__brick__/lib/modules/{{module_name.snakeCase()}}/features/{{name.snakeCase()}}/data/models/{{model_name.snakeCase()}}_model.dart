import 'package:{{project_name.snakeCase()}}/core/extensions/typedefs.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}//features/{{name.snakeCase()}}/domain/entities/{{model_name.snakeCase()}}_entity.dart';

final class {{model_name.pascalCase()}}Model extends {{model_name.pascalCase()}}Entity {
  final String? mId;

  {{model_name.pascalCase()}}Model({
    this.mId,
  }) : super(id: mId ?? '');

  factory {{model_name.pascalCase()}}Model.fromMap(JSON? map) {
    return {{model_name.pascalCase()}}Model(
      mId: map?['id'] ?? '',
    );
  }
}

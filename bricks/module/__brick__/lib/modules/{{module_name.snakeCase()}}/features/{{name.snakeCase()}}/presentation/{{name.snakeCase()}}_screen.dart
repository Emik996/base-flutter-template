import 'package:auto_route/auto_route.dart';
import 'package:{{project_name.snakeCase()}}/core/config/di/injection.dart';
import 'package:{{project_name.snakeCase()}}/core/domain/base_cubit_state.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/domain/entities/{{model_name.snakeCase()}}_entity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'cubits/{{initial_use_case_name.snakeCase()}}_cubit.dart';


@RoutePage()
class {{name.pascalCase()}}Screen extends StatefulWidget {
  const {{name.pascalCase()}}Screen({Key? key}) : super(key: key);

  @override
  State<{{name.pascalCase()}}Screen> createState() => _{{name.pascalCase()}}ScreenState();
}

class _{{name.pascalCase()}}ScreenState extends State<{{name.pascalCase()}}Screen> {
  final _cubit = di<{{initial_use_case_name.pascalCase()}}Cubit>()..{{initial_use_case_name.camelCase()}}();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider.value(
        value: _cubit,
        child: BlocBuilder<{{initial_use_case_name.pascalCase()}}Cubit, SimpleBaseBlocState<{{model_name.pascalCase()}}Entity>>(
          bloc: _cubit,
          builder: (context, state) {
            return const Center();
          },
        ),
      ),
    );
  }
}

base class {{model_name.pascalCase()}}Entity {
  {{model_name.pascalCase()}}Entity({
    required this.id,
  });

  final String id;

  {{model_name.pascalCase()}}Entity copyWith({
    String? id,
  }) {
    return {{model_name.pascalCase()}}Entity(
      id: id ?? this.id,
    );
  }

  @override
  String toString() => '{{model_name.pascalCase()}}Entity(id: $id)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is {{model_name.pascalCase()}}Entity && other.id == id;
  }

  @override
  int get hashCode => id.hashCode;
}

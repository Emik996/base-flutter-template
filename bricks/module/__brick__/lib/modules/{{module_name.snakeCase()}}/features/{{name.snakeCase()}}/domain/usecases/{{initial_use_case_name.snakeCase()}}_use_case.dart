import 'package:{{project_name.snakeCase()}}/core/domain/base_params.dart';
import 'package:{{project_name.snakeCase()}}/core/domain/base_repo_entity.dart';
import 'package:{{project_name.snakeCase()}}/core/domain/base_usecase.dart';
import 'package:{{project_name.snakeCase()}}/core/extensions/typedefs.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/domain/{{module_name.snakeCase()}}_domain_repository.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/domain/entities/{{model_name.snakeCase()}}_entity.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class {{initial_use_case_name.pascalCase()}}UseCase extends BaseUseCase<{{model_name.pascalCase()}}Entity, {{initial_use_case_name.pascalCase()}}Params> {
  {{initial_use_case_name.pascalCase()}}UseCase({required {{module_name.pascalCase()}}DomainRepository repo}) : _repo = repo;
  final {{module_name.pascalCase()}}DomainRepository _repo;

  @override
  Future<BaseResponseEntity<{{model_name.pascalCase()}}Entity>> call({{initial_use_case_name.pascalCase()}}Params? params) {
    return _repo.{{initial_use_case_name.camelCase()}}(params: params);
  }
}

final class {{initial_use_case_name.pascalCase()}}Params extends BaseParams {
  {{initial_use_case_name.pascalCase()}}Params();

  @override
  JSON toJson() {
    return {};
  }
}

import 'package:{{project_name.snakeCase()}}/core/domain/base_repo_entity.dart';
import 'package:{{project_name.snakeCase()}}/core/domain/base_repository.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/domain/entities/{{model_name.snakeCase()}}_entity.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/domain/usecases/{{initial_use_case_name.snakeCase()}}_use_case.dart';

abstract class {{module_name.pascalCase()}}DomainRepository implements BaseRepository {
  Future<BaseResponseEntity<{{model_name.pascalCase()}}Entity>> {{initial_use_case_name.camelCase()}}({
    required {{initial_use_case_name.pascalCase()}}Params? params,
  });
}

import 'package:{{project_name.snakeCase()}}/core/data/network/api_routes.dart' as routes;
import 'package:{{project_name.snakeCase()}}/core/data/network/rest_client.dart';
import 'package:{{project_name.snakeCase()}}/core/domain/base_repo_entity.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/domain/{{module_name.snakeCase()}}_domain_repository.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/data/models/{{model_name.snakeCase()}}_model.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/domain/usecases/{{initial_use_case_name.snakeCase()}}_use_case.dart';
import 'package:injectable/injectable.dart';

@LazySingleton(as: {{module_name.pascalCase()}}DomainRepository)
final class {{module_name.pascalCase()}}Repository implements {{module_name.pascalCase()}}DomainRepository {
  {{module_name.pascalCase()}}Repository({
    required RestClient restClient,
  }) : _client = restClient;

  final RestClient _client;

  @override
  Future<BaseResponseEntity<{{model_name.pascalCase()}}Model>> {{initial_use_case_name.camelCase()}}({
    required {{initial_use_case_name.pascalCase()}}Params? params,
  }) async {
    return await _client.call<{{model_name.pascalCase()}}Model>(
      RestMethod.post,
      routes.{{api_route.camelCase()}},
      data: params?.toJson(),
      jsonToModel: {{model_name.pascalCase()}}Model.fromMap,
    );
  }
}

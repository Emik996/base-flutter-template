import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/domain/{{module_name.snakeCase()}}_domain_repository.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/domain/usecases/{{initial_use_case_name.snakeCase()}}_use_case.dart';

{{module_name.pascalCase()}}DomainRepository,
{{initial_use_case_name.pascalCase()}}UseCase,
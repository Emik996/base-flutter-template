import 'dart:convert';
import 'package:{{project_name.snakeCase()}}/core/domain/base_error_entity.dart';
import 'package:{{project_name.snakeCase()}}/core/domain/base_repo_entity.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/data/models/{{model_name.snakeCase()}}_model.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/domain/entities/{{model_name.snakeCase()}}_entity.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/domain/usecases/{{initial_use_case_name.snakeCase()}}_use_case.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import '../../../../../fixtures/fixture_reader.dart';
import '../../../../../helpers/test_helper.mocks.dart';

void main() {
  late Mock{{module_name.pascalCase()}}DomainRepository mock{{module_name.pascalCase()}}DomainRepository;
  late {{initial_use_case_name.pascalCase()}}UseCase usecase;
  final testJson = json.decode(fixtureReader('{{model_name.snakeCase()}}_model.json'));

  setUp(() {
    mock{{module_name.pascalCase()}}DomainRepository = Mock{{module_name.pascalCase()}}DomainRepository();
    usecase = {{initial_use_case_name.pascalCase()}}UseCase(repo: mock{{module_name.pascalCase()}}DomainRepository);
  });

  test('Should return {{model_name.pascalCase()}}Model when call {{module_name.lowerCase()}} repository', () async {
    //arrange
    when(
      mock{{module_name.pascalCase()}}DomainRepository.{{initial_use_case_name.camelCase()}}(params: anyNamed('params')),
    ).thenAnswer(
      (_) async => BaseResponseEntity<{{model_name.pascalCase()}}Entity>(
        model: {{model_name.pascalCase()}}Model.fromMap(testJson),
      ),
    );
    //actual
    final result = await usecase({{initial_use_case_name.pascalCase()}}Params());

    expect(result.model?.id, '1');
  });

  test('Should return CustomErrorEntity when call {{module_name.lowerCase()}} repository', () async {
    //arrange
    when(
      mock{{module_name.pascalCase()}}DomainRepository.{{initial_use_case_name.camelCase()}}(params: anyNamed('params')),
    ).thenAnswer(
      (_) async => BaseResponseEntity<{{model_name.pascalCase()}}Entity>(
        error: CustomErrorEntity(title: 'Error'),
      ),
    );
    //actual
    final result = await usecase({{initial_use_case_name.pascalCase()}}Params());

    expect(result.error?.exception.title, 'Error');
  });
}

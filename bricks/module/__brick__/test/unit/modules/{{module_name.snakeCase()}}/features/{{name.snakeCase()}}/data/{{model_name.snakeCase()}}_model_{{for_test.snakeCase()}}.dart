import 'dart:convert';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/data/models/{{model_name.snakeCase()}}_model.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/domain/entities/{{model_name.snakeCase()}}_entity.dart';
import 'package:flutter_test/flutter_test.dart';
import '../../../../../fixtures/fixture_reader.dart';

void main() {
  final testJson = fixtureReader('{{model_name.snakeCase()}}_model.json');
  final {{model_name.snakeCase()}}Model = {{model_name.pascalCase()}}Model(mId: '1');

  test('Should be a subclass of {{model_name.pascalCase()}}Entity', () async {
    expect({{model_name.snakeCase()}}Model, isA<{{model_name.pascalCase()}}Entity>());
  });

  test('fromJson', () async {
    //arrange
    final Map<String, dynamic> jsonMap = json.decode(testJson);
    // act
    final result = {{model_name.pascalCase()}}Model.fromMap(jsonMap);
    // assert
    expect(result, {{model_name.snakeCase()}}Model);
  });
}

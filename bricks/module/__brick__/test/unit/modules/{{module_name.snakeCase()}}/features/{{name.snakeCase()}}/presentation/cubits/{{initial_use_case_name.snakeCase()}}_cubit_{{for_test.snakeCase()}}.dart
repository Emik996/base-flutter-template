import 'dart:convert';

import 'package:{{project_name.snakeCase()}}/core/domain/base_cubit_state.dart';
import 'package:{{project_name.snakeCase()}}/core/domain/base_error_entity.dart';
import 'package:{{project_name.snakeCase()}}/core/domain/base_repo_entity.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/data/models/{{model_name.snakeCase()}}_model.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/domain/entities/{{model_name.snakeCase()}}_entity.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/domain/usecases/{{initial_use_case_name.snakeCase()}}_use_case.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/presentation/cubits/{{initial_use_case_name.snakeCase()}}_cubit.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../../../../../fixtures/fixture_reader.dart';
import '../../../../../../helpers/test_helper.mocks.dart';

void main() {
  late {{initial_use_case_name.pascalCase()}}Cubit cubit;
  late Mock{{initial_use_case_name.pascalCase()}}UseCase {{initial_use_case_name.camelCase()}}UseCase;

  final testJson = json.decode(fixtureReader('{{model_name.snakeCase()}}_model.json'));
  final params = {{initial_use_case_name.pascalCase()}}Params();

  setUp(() {
    {{initial_use_case_name.camelCase()}}UseCase = Mock{{initial_use_case_name.pascalCase()}}UseCase();
    cubit = {{initial_use_case_name.pascalCase()}}Cubit({{initial_use_case_name.camelCase()}}UseCase: {{initial_use_case_name.camelCase()}}UseCase);
  });

  test('Initial state must have StateStatus.initial', () {
    // assert
    expect(cubit.state.status, StateStatus.initial);
  });

  blocTest<{{initial_use_case_name.pascalCase()}}Cubit, SimpleBaseBlocState<{{model_name.pascalCase()}}Entity>>(
    'Emits loading -> Runs UseCase -> Emits loaded state with a data',
    build: () => cubit,
    setUp: () {
      when({{initial_use_case_name.camelCase()}}UseCase.call(params)).thenAnswer(
        (_) async => BaseResponseEntity<{{model_name.pascalCase()}}Entity>(
          model: {{model_name.pascalCase()}}Model.fromMap(testJson),
        ),
      );
    },
    act: (cubit) => cubit.{{initial_use_case_name.camelCase()}}(params: params),
    expect: () => [
      const SimpleBaseBlocState<{{model_name.pascalCase()}}Entity>.loading(),
      SimpleBaseBlocState<{{model_name.pascalCase()}}Entity>.loaded({{model_name.pascalCase()}}Model.fromMap(testJson)),
    ],
    verify: (_) {
      verify({{initial_use_case_name.camelCase()}}UseCase.call(params));
    },
  );

  blocTest<{{initial_use_case_name.pascalCase()}}Cubit, SimpleBaseBlocState<{{model_name.pascalCase()}}Entity>>(
    'Emits loading -> Runs UseCase -> Emits error state with a error entity',
    build: () => cubit,
    setUp: () {
      when({{initial_use_case_name.camelCase()}}UseCase.call(params)).thenAnswer(
        (_) async => BaseResponseEntity<{{model_name.pascalCase()}}Entity>(
          error: CustomErrorEntity(
            statusCode: 500,
            title: 'Internal Server Error',
          ),
        ),
      );
    },
    act: (cubit) => cubit.{{initial_use_case_name.camelCase()}}(params: params),
    expect: () => [
      const SimpleBaseBlocState<{{model_name.pascalCase()}}Entity>.loading(),
      SimpleBaseBlocState<{{model_name.pascalCase()}}Entity>.error(
        CustomErrorEntity(
          statusCode: 500,
          title: 'Internal Server Error',
        ),
      ),
    ],
    verify: (_) {
      verify({{initial_use_case_name.camelCase()}}UseCase.call(params));
    },
  );
}

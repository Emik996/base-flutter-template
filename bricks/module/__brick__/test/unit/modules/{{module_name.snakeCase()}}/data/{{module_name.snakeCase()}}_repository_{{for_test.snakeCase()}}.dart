import 'dart:convert';
import 'package:{{project_name.snakeCase()}}/core/domain/base_error_entity.dart';
import 'package:{{project_name.snakeCase()}}/core/domain/base_repo_entity.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/data/{{module_name.snakeCase()}}_repository.dart';
import 'package:{{project_name.snakeCase()}}/modules/{{module_name.snakeCase()}}/features/{{name.snakeCase()}}/data/models/{{model_name.snakeCase()}}_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import '../../../fixtures/fixture_reader.dart';
import '../../../helpers/test_helper.mocks.dart';

void main() {
  late MockRestClient restClient;
  late {{module_name.pascalCase()}}Repository {{module_name.camelCase()}}Repository;

  final {{model_name.camelCase()}}EntityJson = json.decode(fixtureReader('{{model_name.snakeCase()}}_model.json'));

  setUp(() {
    restClient = MockRestClient();
    {{module_name.camelCase()}}Repository = {{module_name.pascalCase()}}Repository(restClient: restClient);
  });

  test(
      'Should return {{model_name.pascalCase()}}Model when call rest client service but error is null',
      () async {
    //arrange
    when(
      restClient.call<{{model_name.pascalCase()}}Model>(
        any,
        any,
        jsonToModel: {{model_name.pascalCase()}}Model.fromMap,
      ),
    ).thenAnswer(
      (_) async => BaseResponseEntity<{{model_name.pascalCase()}}Model>(
        model: {{model_name.pascalCase()}}Model.fromMap({{model_name.camelCase()}}EntityJson),
      ),
    );

    //act
    final result = await {{module_name.camelCase()}}Repository.{{initial_use_case_name.camelCase()}}(params: null);

    //assert
    verify(
      restClient.call<{{model_name.pascalCase()}}Model>(
        any,
        any,
        jsonToModel: {{model_name.pascalCase()}}Model.fromMap,
      ),
    );
    expect(result.model, equals({{model_name.pascalCase()}}Model.fromMap({{model_name.camelCase()}}EntityJson)));
    expect(result.error, null);
  });

  test(
      'Should return CustomErrorEntity when call rest client service but model is null',
      () async {
    //arrange
    when(
      restClient.call<{{model_name.pascalCase()}}Model>(
        any,
        any,
        jsonToModel: {{model_name.pascalCase()}}Model.fromMap,
      ),
    ).thenAnswer(
      (_) async => BaseResponseEntity<{{model_name.pascalCase()}}Model>(
        error: CustomErrorEntity(
          statusCode: 500,
          title: 'Internal Server Error',
        ),
      ),
    );

    //act
    final result = await {{module_name.camelCase()}}Repository.{{initial_use_case_name.camelCase()}}(params: null);

    //assert
    verify(
      restClient.call<{{model_name.pascalCase()}}Model>(
        any,
        any,
        jsonToModel: {{model_name.pascalCase()}}Model.fromMap,
      ),
    );
    expect(result.error?.exception.statusCode, 500);
    expect(result.model, null);
  });
}

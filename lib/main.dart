import 'package:base_flutter/core/config/flavor_configuration.dart';
import 'package:base_flutter/modules/main_app_builder.dart';
import 'package:base_flutter/modules/main_app_runner.dart';

void main() {
  final runner = MainAppRunner();
  final builder = MainAppBuilder();
  runner.run(builder, Flavor.dev);
}

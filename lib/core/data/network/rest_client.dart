import 'dart:io';
import 'package:base_flutter/core/config/flavor_configuration.dart';
import 'package:base_flutter/core/data/network/interceptors/base_interceptors.dart';
import 'package:base_flutter/core/data/network/network_info.dart';
import 'package:base_flutter/core/domain/base_error_entity.dart';
import 'package:base_flutter/core/domain/base_repo_entity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';

enum RestMethod { get, post, put, delete, patch }

enum ServiceType { base }

@lazySingleton
class RestClient with DioMixin implements Dio {
  RestClient({
    required NetworkInfo networkInfo,
    required BaseInterceptors baseInterceptors,
  })  : _networkInfo = networkInfo,
        _baseInterceptors = baseInterceptors {
    options = BaseOptions(
        baseUrl: AppFlavors.baseUrl,
        connectTimeout: const Duration(seconds: 30),
        receiveTimeout: const Duration(seconds: 30),
        sendTimeout: const Duration(seconds: 30),
        contentType: 'application/json');
    httpClientAdapter = HttpClientAdapter();
    _setInterceptors(ServiceType.base);
  }

  final NetworkInfo _networkInfo;
  final BaseInterceptors _baseInterceptors;

  Future<BaseResponseEntity<M>> call<M>(
    RestMethod method,
    String path, {
    ServiceType serviceType = ServiceType.base,
    dynamic data,
    dynamic parametres,
    CancelToken? cancelToken,
    Options? options,
    required M Function(Map<String, dynamic> map) jsonToModel,
    Function(int, int)? onSendProgress,
  }) async {
    if (await _networkInfo.isConnected) {
      try {
        final response = await request(
          path,
          queryParameters: parametres,
          data: data,
          cancelToken: cancelToken,
          onSendProgress: onSendProgress,
          options: (options ?? Options()).copyWith(
            method: method.name.toUpperCase(),
          ),
        );

        return BaseResponseEntity<M>(model: jsonToModel(response.data));
      } on DioException catch (exception) {
        return BaseResponseEntity(
          error: DioErrorEntity(response: exception.response),
        );
      } on SocketException {
        return BaseResponseEntity(error: NoConnectionErrorEntity());
      } catch (exception) {
        return BaseResponseEntity(error: NoConnectionErrorEntity());
      }
    } else {
      return BaseResponseEntity(error: NoConnectionErrorEntity());
    }
  }

  void _setInterceptors(ServiceType serviceType) {
    interceptors.clear();
    if (kDebugMode) {
      interceptors.add(LogInterceptor(
        error: true,
        request: true,
        requestBody: true,
        requestHeader: true,
        responseBody: true,
        responseHeader: true,
      ));
    }

    switch (serviceType) {
      default:
        interceptors.add(_baseInterceptors);
        break;
    }
  }
}

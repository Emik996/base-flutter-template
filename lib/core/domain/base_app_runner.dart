import 'package:base_flutter/core/config/flavor_configuration.dart';
import 'base_app_builder.dart';

abstract interface class BaseAppRunner {
  Future<void> preloadData();
  Future<void> run(BaseAppBuilder builder, Flavor flavor);
}

abstract interface class BaseAppBuilder {
  dynamic buildApp();
}

import 'base_params.dart';
import 'base_repo_entity.dart';

abstract class BaseUseCase<Type, Params extends BaseParams> {
  Future<BaseResponseEntity<Type>> call(Params params);
}

abstract class BaseNoParamsUseCase<Type extends Object> {
  Future<BaseResponseEntity<Type>> call();
}

import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/modules/auhorization/features/login/presentation/login_screen.dart';
import 'package:base_flutter/modules/splash/splash_screen.dart';

part 'router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(page: SplashRoute.page, initial: true),
        AutoRoute(page: LoginRoute.page),
      ];
}

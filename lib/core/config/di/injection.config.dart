// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:connectivity_plus/connectivity_plus.dart' as _i5;
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as _i7;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i8;

import '../../../modules/auhorization/data/auth_repository.dart' as _i14;
import '../../../modules/auhorization/domain/auth_domain_repository.dart'
    as _i13;
import '../../../modules/auhorization/features/login/domain/usecases/login_usecase.dart'
    as _i15;
import '../../../modules/auhorization/features/login/presentation/cubits/login_cubit.dart'
    as _i16;
import '../../../modules/splash/cubits/splash_cubit.dart' as _i9;
import '../../data/network/interceptors/base_interceptors.dart' as _i10;
import '../../data/network/network_info.dart' as _i4;
import '../../data/network/rest_client.dart' as _i12;
import '../../data/storage/secure_storage.dart' as _i6;
import '../../data/storage/shared_preferences.dart' as _i17;
import '../../global_observer.dart' as _i3;
import '../../presentation/cubits/connection_cubit.dart' as _i11;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  Future<_i1.GetIt> init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) async {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final registerModule = _$RegisterModule();
    gh.factory<_i3.GlobalObserver>(() => _i3.GlobalObserver());
    gh.lazySingleton<_i4.NetworkInfo>(
        () => _i4.NetworkInfoImpl(connectivity: gh<_i5.Connectivity>()));
    gh.lazySingleton<_i6.SecureStorage>(
        () => _i6.SecureStorage(storage: gh<_i7.FlutterSecureStorage>()));
    await gh.factoryAsync<_i8.SharedPreferences>(
      () => registerModule.prefs,
      preResolve: true,
    );
    gh.factory<_i9.SplashCubit>(
        () => _i9.SplashCubit(prefs: gh<_i8.SharedPreferences>()));
    gh.lazySingleton<_i10.BaseInterceptors>(
        () => _i10.BaseInterceptors(prefs: gh<_i8.SharedPreferences>()));
    gh.factory<_i11.ConnectionCubit>(
        () => _i11.ConnectionCubit(networkInfo: gh<_i4.NetworkInfo>()));
    gh.lazySingleton<_i12.RestClient>(() => _i12.RestClient(
          networkInfo: gh<_i4.NetworkInfo>(),
          baseInterceptors: gh<_i10.BaseInterceptors>(),
        ));
    gh.lazySingleton<_i13.AuthDomainRepository>(
        () => _i14.AuthRepository(restClient: gh<_i12.RestClient>()));
    gh.lazySingleton<_i15.LoginUseCase>(
        () => _i15.LoginUseCase(repo: gh<_i13.AuthDomainRepository>()));
    gh.factory<_i16.LoginCubit>(
        () => _i16.LoginCubit(loginUseCase: gh<_i15.LoginUseCase>()));
    return this;
  }
}

class _$RegisterModule extends _i17.RegisterModule {}

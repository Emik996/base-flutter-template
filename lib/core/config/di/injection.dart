import 'package:base_flutter/core/config/di/injection.config.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

final di = GetIt.instance;

@InjectableInit(preferRelativeImports: true)
configureDependencies(
  GetIt getIt, {
  String? env,
  EnvironmentFilter? environmentFilter,
}) {
  getIt.registerLazySingleton<Connectivity>(
    () => Connectivity(),
  );

  getIt.registerLazySingleton<FlutterSecureStorage>(
    () => const FlutterSecureStorage(),
  );

  return getIt.init(
    environmentFilter: environmentFilter,
    environment: env,
  );
}

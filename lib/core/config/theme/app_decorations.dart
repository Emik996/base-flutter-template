part of 'app_theme.dart';

abstract base class AppDecorations {
  final BoxDecoration backgroundDecoration;

  AppDecorations({required this.backgroundDecoration});
}

final class LightDecorations implements AppDecorations {
  @override
  BoxDecoration get backgroundDecoration => const BoxDecoration();
}

final class DarkDecorations implements AppDecorations {
  @override
  BoxDecoration get backgroundDecoration => const BoxDecoration();
}

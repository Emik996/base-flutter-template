part of 'app_theme.dart';

abstract class AppTextStyles {
  static final double _headerLargerFS = ScreenUtil().setSp(27);
  // static final double _titleLargeFS = ScreenUtil().setSp(26);
  // static final double _titleMediumFS = ScreenUtil().setSp(20);
  // static final double _titleSmallFS = ScreenUtil().setSp(18);
  // static final double _bodyMediumFS = ScreenUtil().setSp(16);
  // static final double _bodySmallFS = ScreenUtil().setSp(14);
  // static final double _bodyExtraSmallFS = ScreenUtil().setSp(12);

  ///TITLE

  static TextStyle firasans27w400 = TextStyle(
    fontSize: _headerLargerFS,
    fontWeight: FontWeight.w400,
    fontFamily: 'FiraSans',
    height: 1.2,
  );

  ///BODY

  ///LABEL
}

// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class L10n {
  L10n();

  static L10n? _current;

  static L10n get current {
    assert(_current != null,
        'No instance of L10n was loaded. Try to initialize the L10n delegate before accessing L10n.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<L10n> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = L10n();
      L10n._current = instance;

      return instance;
    });
  }

  static L10n of(BuildContext context) {
    final instance = L10n.maybeOf(context);
    assert(instance != null,
        'No instance of L10n present in the widget tree. Did you add L10n.delegate in localizationsDelegates?');
    return instance!;
  }

  static L10n? maybeOf(BuildContext context) {
    return Localizations.of<L10n>(context, L10n);
  }

  /// `Что то пошло не так`
  String get smthGoneWrong {
    return Intl.message(
      'Что то пошло не так',
      name: 'smthGoneWrong',
      desc: '',
      args: [],
    );
  }

  /// `Нет интернет соединения`
  String get noInternet {
    return Intl.message(
      'Нет интернет соединения',
      name: 'noInternet',
      desc: '',
      args: [],
    );
  }

  /// `Проверьте, пожалуйста, свое соединение с интернетом!`
  String get checkYourConnection {
    return Intl.message(
      'Проверьте, пожалуйста, свое соединение с интернетом!',
      name: 'checkYourConnection',
      desc: '',
      args: [],
    );
  }

  /// `Истекло время ожидания`
  String get timeOut {
    return Intl.message(
      'Истекло время ожидания',
      name: 'timeOut',
      desc: '',
      args: [],
    );
  }

  /// `Перезагрузите страницу!`
  String get refreshPage {
    return Intl.message(
      'Перезагрузите страницу!',
      name: 'refreshPage',
      desc: '',
      args: [],
    );
  }

  /// `Пустая страница.`
  String get emptyPage {
    return Intl.message(
      'Пустая страница.',
      name: 'emptyPage',
      desc: '',
      args: [],
    );
  }

  /// `Некорректные данные.`
  String get incorrectData {
    return Intl.message(
      'Некорректные данные.',
      name: 'incorrectData',
      desc: '',
      args: [],
    );
  }

  /// `Кажется, ваше подключение истекло!`
  String get yourConnectionIsGone {
    return Intl.message(
      'Кажется, ваше подключение истекло!',
      name: 'yourConnectionIsGone',
      desc: '',
      args: [],
    );
  }

  /// `Ошибка сервера.`
  String get serverError {
    return Intl.message(
      'Ошибка сервера.',
      name: 'serverError',
      desc: '',
      args: [],
    );
  }

  /// `Попробуйте, пожалуйста, через некоторое время!`
  String get tryAgainLater {
    return Intl.message(
      'Попробуйте, пожалуйста, через некоторое время!',
      name: 'tryAgainLater',
      desc: '',
      args: [],
    );
  }

  /// `Поле должно содержать 16 символов.`
  String get mustContains16Symbols {
    return Intl.message(
      'Поле должно содержать 16 символов.',
      name: 'mustContains16Symbols',
      desc: '',
      args: [],
    );
  }

  /// `Поле не может быть пустым.`
  String get cantBeEmpty {
    return Intl.message(
      'Поле не может быть пустым.',
      name: 'cantBeEmpty',
      desc: '',
      args: [],
    );
  }

  /// `РНМ`
  String get rnm {
    return Intl.message(
      'РНМ',
      name: 'rnm',
      desc: '',
      args: [],
    );
  }

  /// `Пароль`
  String get password {
    return Intl.message(
      'Пароль',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `Войти`
  String get comeIn {
    return Intl.message(
      'Войти',
      name: 'comeIn',
      desc: '',
      args: [],
    );
  }

  /// `Вход`
  String get input {
    return Intl.message(
      'Вход',
      name: 'input',
      desc: '',
      args: [],
    );
  }

  /// `Пароль введен неверно`
  String get incorrectPassword {
    return Intl.message(
      'Пароль введен неверно',
      name: 'incorrectPassword',
      desc: '',
      args: [],
    );
  }

  /// `Введите пароль`
  String get enterPassword {
    return Intl.message(
      'Введите пароль',
      name: 'enterPassword',
      desc: '',
      args: [],
    );
  }

  /// `Введите код для доступа`
  String get enterPincode {
    return Intl.message(
      'Введите код для доступа',
      name: 'enterPincode',
      desc: '',
      args: [],
    );
  }

  /// `Создайте код для доступа`
  String get cretePincode {
    return Intl.message(
      'Создайте код для доступа',
      name: 'cretePincode',
      desc: '',
      args: [],
    );
  }

  /// `Повторите код доступа`
  String get repeatPincode {
    return Intl.message(
      'Повторите код доступа',
      name: 'repeatPincode',
      desc: '',
      args: [],
    );
  }

  /// `Неверный код доступа`
  String get unvalidPincode {
    return Intl.message(
      'Неверный код доступа',
      name: 'unvalidPincode',
      desc: '',
      args: [],
    );
  }

  /// `Создайте 4-значный пин-код`
  String get createPincode {
    return Intl.message(
      'Создайте 4-значный пин-код',
      name: 'createPincode',
      desc: '',
      args: [],
    );
  }

  /// `Пин-коды не совпадают`
  String get pincodesNotEqual {
    return Intl.message(
      'Пин-коды не совпадают',
      name: 'pincodesNotEqual',
      desc: '',
      args: [],
    );
  }

  /// `Забыли пин-код?`
  String get forgotPincode {
    return Intl.message(
      'Забыли пин-код?',
      name: 'forgotPincode',
      desc: '',
      args: [],
    );
  }

  /// `Продажа`
  String get sell {
    return Intl.message(
      'Продажа',
      name: 'sell',
      desc: '',
      args: [],
    );
  }

  /// `Безналичные`
  String get nonCash {
    return Intl.message(
      'Безналичные',
      name: 'nonCash',
      desc: '',
      args: [],
    );
  }

  /// `Наличные`
  String get cash {
    return Intl.message(
      'Наличные',
      name: 'cash',
      desc: '',
      args: [],
    );
  }

  /// `Принято`
  String get received {
    return Intl.message(
      'Принято',
      name: 'received',
      desc: '',
      args: [],
    );
  }

  /// `Сдача`
  String get odd {
    return Intl.message(
      'Сдача',
      name: 'odd',
      desc: '',
      args: [],
    );
  }

  /// `Оплатить`
  String get makePayment {
    return Intl.message(
      'Оплатить',
      name: 'makePayment',
      desc: '',
      args: [],
    );
  }

  /// `Возврат продажи`
  String get saleReturn {
    return Intl.message(
      'Возврат продажи',
      name: 'saleReturn',
      desc: '',
      args: [],
    );
  }

  /// `Текущая смена`
  String get currentCashbox {
    return Intl.message(
      'Текущая смена',
      name: 'currentCashbox',
      desc: '',
      args: [],
    );
  }

  /// `Смена и чеки`
  String get cashboxAndReceipts {
    return Intl.message(
      'Смена и чеки',
      name: 'cashboxAndReceipts',
      desc: '',
      args: [],
    );
  }

  /// `Отчеты`
  String get reports {
    return Intl.message(
      'Отчеты',
      name: 'reports',
      desc: '',
      args: [],
    );
  }

  /// `Настройки`
  String get settings {
    return Intl.message(
      'Настройки',
      name: 'settings',
      desc: '',
      args: [],
    );
  }

  /// `Выход`
  String get exit {
    return Intl.message(
      'Выход',
      name: 'exit',
      desc: '',
      args: [],
    );
  }

  /// `Проверка не доступна`
  String get checkingNotAvailable {
    return Intl.message(
      'Проверка не доступна',
      name: 'checkingNotAvailable',
      desc: '',
      args: [],
    );
  }

  /// `Устройство не поддерживается`
  String get deviceNotSupported {
    return Intl.message(
      'Устройство не поддерживается',
      name: 'deviceNotSupported',
      desc: '',
      args: [],
    );
  }

  /// `Не поддерживаемая ОС`
  String get osNotSupported {
    return Intl.message(
      'Не поддерживаемая ОС',
      name: 'osNotSupported',
      desc: '',
      args: [],
    );
  }

  /// `Не установлен PIN-code`
  String get pinNotInstalled {
    return Intl.message(
      'Не установлен PIN-code',
      name: 'pinNotInstalled',
      desc: '',
      args: [],
    );
  }

  /// `Пройдите биометрию для входа в приложение`
  String get passBioForEnter {
    return Intl.message(
      'Пройдите биометрию для входа в приложение',
      name: 'passBioForEnter',
      desc: '',
      args: [],
    );
  }

  /// `Произошла системная ошибка`
  String get systemError {
    return Intl.message(
      'Произошла системная ошибка',
      name: 'systemError',
      desc: '',
      args: [],
    );
  }

  /// `Чек`
  String get receipt {
    return Intl.message(
      'Чек',
      name: 'receipt',
      desc: '',
      args: [],
    );
  }

  /// `На данный момент ГНС недоступен`
  String get gnsNotAvailable {
    return Intl.message(
      'На данный момент ГНС недоступен',
      name: 'gnsNotAvailable',
      desc: '',
      args: [],
    );
  }

  /// `Попробуйте заново`
  String get tryAgain {
    return Intl.message(
      'Попробуйте заново',
      name: 'tryAgain',
      desc: '',
      args: [],
    );
  }

  /// `Принтер не обнаружен`
  String get printetNotFound {
    return Intl.message(
      'Принтер не обнаружен',
      name: 'printetNotFound',
      desc: '',
      args: [],
    );
  }

  /// `Проверьте свое подключение`
  String get checkYourPrinterConnection {
    return Intl.message(
      'Проверьте свое подключение',
      name: 'checkYourPrinterConnection',
      desc: '',
      args: [],
    );
  }

  /// `Принтер`
  String get printer {
    return Intl.message(
      'Принтер',
      name: 'printer',
      desc: '',
      args: [],
    );
  }

  /// `Подключение принтера`
  String get printerConnection {
    return Intl.message(
      'Подключение принтера',
      name: 'printerConnection',
      desc: '',
      args: [],
    );
  }

  /// `Поиск`
  String get searching {
    return Intl.message(
      'Поиск',
      name: 'searching',
      desc: '',
      args: [],
    );
  }

  /// `Принтеры не найдены.`
  String get printersNotFound {
    return Intl.message(
      'Принтеры не найдены.',
      name: 'printersNotFound',
      desc: '',
      args: [],
    );
  }

  /// `Проверьте ваше подключение к Bluetooth.`
  String get checkYourBluetoothConnection {
    return Intl.message(
      'Проверьте ваше подключение к Bluetooth.',
      name: 'checkYourBluetoothConnection',
      desc: '',
      args: [],
    );
  }

  /// `Ошибка подключения`
  String get connectionError {
    return Intl.message(
      'Ошибка подключения',
      name: 'connectionError',
      desc: '',
      args: [],
    );
  }

  /// `Безопасность`
  String get safety {
    return Intl.message(
      'Безопасность',
      name: 'safety',
      desc: '',
      args: [],
    );
  }

  /// `Выбор языка`
  String get languageSelection {
    return Intl.message(
      'Выбор языка',
      name: 'languageSelection',
      desc: '',
      args: [],
    );
  }

  /// `Смена`
  String get cashbox {
    return Intl.message(
      'Смена',
      name: 'cashbox',
      desc: '',
      args: [],
    );
  }

  /// `Безналичная оплата`
  String get cashlessPayment {
    return Intl.message(
      'Безналичная оплата',
      name: 'cashlessPayment',
      desc: '',
      args: [],
    );
  }

  /// `Настройки цвета`
  String get colorsettings {
    return Intl.message(
      'Настройки цвета',
      name: 'colorsettings',
      desc: '',
      args: [],
    );
  }

  /// `Использовать биометрию для входа?`
  String get shouldUseBio {
    return Intl.message(
      'Использовать биометрию для входа?',
      name: 'shouldUseBio',
      desc: '',
      args: [],
    );
  }

  /// `Нет`
  String get no {
    return Intl.message(
      'Нет',
      name: 'no',
      desc: '',
      args: [],
    );
  }

  /// `Да`
  String get yes {
    return Intl.message(
      'Да',
      name: 'yes',
      desc: '',
      args: [],
    );
  }

  /// `Отменить`
  String get cancel {
    return Intl.message(
      'Отменить',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Светлая тема`
  String get whiteTheme {
    return Intl.message(
      'Светлая тема',
      name: 'whiteTheme',
      desc: '',
      args: [],
    );
  }

  /// `Темная тема`
  String get darkTheme {
    return Intl.message(
      'Темная тема',
      name: 'darkTheme',
      desc: '',
      args: [],
    );
  }

  /// `Вы хотите выйти из приложения?`
  String get doYouWantExit {
    return Intl.message(
      'Вы хотите выйти из приложения?',
      name: 'doYouWantExit',
      desc: '',
      args: [],
    );
  }

  /// `Печать`
  String get print {
    return Intl.message(
      'Печать',
      name: 'print',
      desc: '',
      args: [],
    );
  }

  /// `Распечатать`
  String get print2 {
    return Intl.message(
      'Распечатать',
      name: 'print2',
      desc: '',
      args: [],
    );
  }

  /// `Поделиться`
  String get share {
    return Intl.message(
      'Поделиться',
      name: 'share',
      desc: '',
      args: [],
    );
  }

  /// `Чеки`
  String get receipts {
    return Intl.message(
      'Чеки',
      name: 'receipts',
      desc: '',
      args: [],
    );
  }

  /// `Помощь`
  String get help {
    return Intl.message(
      'Помощь',
      name: 'help',
      desc: '',
      args: [],
    );
  }

  /// `Свяжитесь с поддержкой`
  String get connectWithSupport {
    return Intl.message(
      'Свяжитесь с поддержкой',
      name: 'connectWithSupport',
      desc: '',
      args: [],
    );
  }

  /// `Произошла ошибка`
  String get anErrorHasOccured {
    return Intl.message(
      'Произошла ошибка',
      name: 'anErrorHasOccured',
      desc: '',
      args: [],
    );
  }

  /// `Не удалось загрузить. Повторите еще раз`
  String get failedToLoadTryAgain {
    return Intl.message(
      'Не удалось загрузить. Повторите еще раз',
      name: 'failedToLoadTryAgain',
      desc: '',
      args: [],
    );
  }

  /// `Повторить`
  String get repeat {
    return Intl.message(
      'Повторить',
      name: 'repeat',
      desc: '',
      args: [],
    );
  }

  /// `Тариф активен до`
  String get tariffActiveUntil {
    return Intl.message(
      'Тариф активен до',
      name: 'tariffActiveUntil',
      desc: '',
      args: [],
    );
  }

  /// `Баланс`
  String get balance {
    return Intl.message(
      'Баланс',
      name: 'balance',
      desc: '',
      args: [],
    );
  }

  /// `ЦТО`
  String get cto {
    return Intl.message(
      'ЦТО',
      name: 'cto',
      desc: '',
      args: [],
    );
  }

  /// `т.`
  String get phone {
    return Intl.message(
      'т.',
      name: 'phone',
      desc: '',
      args: [],
    );
  }

  /// `Дата выпуска`
  String get releaseDate {
    return Intl.message(
      'Дата выпуска',
      name: 'releaseDate',
      desc: '',
      args: [],
    );
  }

  /// `Версия`
  String get version {
    return Intl.message(
      'Версия',
      name: 'version',
      desc: '',
      args: [],
    );
  }

  /// `Возврат чека`
  String get receiptReturn {
    return Intl.message(
      'Возврат чека',
      name: 'receiptReturn',
      desc: '',
      args: [],
    );
  }

  /// `Позвонить`
  String get makeCall {
    return Intl.message(
      'Позвонить',
      name: 'makeCall',
      desc: '',
      args: [],
    );
  }

  /// `Отпечаток пальцев`
  String get fingerprint {
    return Intl.message(
      'Отпечаток пальцев',
      name: 'fingerprint',
      desc: '',
      args: [],
    );
  }

  /// `Не удалось провести оплату`
  String get failedToMakePayment {
    return Intl.message(
      'Не удалось провести оплату',
      name: 'failedToMakePayment',
      desc: '',
      args: [],
    );
  }

  /// `Нет данных`
  String get noData {
    return Intl.message(
      'Нет данных',
      name: 'noData',
      desc: '',
      args: [],
    );
  }

  /// `Поиск по номеру чека`
  String get searchByReceiptNumber {
    return Intl.message(
      'Поиск по номеру чека',
      name: 'searchByReceiptNumber',
      desc: '',
      args: [],
    );
  }

  /// `Продажи`
  String get sells {
    return Intl.message(
      'Продажи',
      name: 'sells',
      desc: '',
      args: [],
    );
  }

  /// `Количество`
  String get quantity {
    return Intl.message(
      'Количество',
      name: 'quantity',
      desc: '',
      args: [],
    );
  }

  /// `Сумма`
  String get summ {
    return Intl.message(
      'Сумма',
      name: 'summ',
      desc: '',
      args: [],
    );
  }

  /// `Количество возвратов`
  String get returnQuantity {
    return Intl.message(
      'Количество возвратов',
      name: 'returnQuantity',
      desc: '',
      args: [],
    );
  }

  /// `Сумма возвратов`
  String get returnSumm {
    return Intl.message(
      'Сумма возвратов',
      name: 'returnSumm',
      desc: '',
      args: [],
    );
  }

  /// `Закрыть рабочую\nсмену`
  String get closeShift {
    return Intl.message(
      'Закрыть рабочую\nсмену',
      name: 'closeShift',
      desc: '',
      args: [],
    );
  }

  /// `Открыть рабочую\nсмену`
  String get openShift {
    return Intl.message(
      'Открыть рабочую\nсмену',
      name: 'openShift',
      desc: '',
      args: [],
    );
  }

  /// `Наличные в кассе`
  String get cashInDesc {
    return Intl.message(
      'Наличные в кассе',
      name: 'cashInDesc',
      desc: '',
      args: [],
    );
  }

  /// `Выручка`
  String get proceeds {
    return Intl.message(
      'Выручка',
      name: 'proceeds',
      desc: '',
      args: [],
    );
  }

  /// `Состояние`
  String get state {
    return Intl.message(
      'Состояние',
      name: 'state',
      desc: '',
      args: [],
    );
  }

  /// `Открыта`
  String get opens {
    return Intl.message(
      'Открыта',
      name: 'opens',
      desc: '',
      args: [],
    );
  }

  /// `Необходимо закрыть`
  String get shouldClose {
    return Intl.message(
      'Необходимо закрыть',
      name: 'shouldClose',
      desc: '',
      args: [],
    );
  }

  /// `Осталось времени`
  String get timeLeft {
    return Intl.message(
      'Осталось времени',
      name: 'timeLeft',
      desc: '',
      args: [],
    );
  }

  /// `Внесение средств`
  String get depositMoeny {
    return Intl.message(
      'Внесение средств',
      name: 'depositMoeny',
      desc: '',
      args: [],
    );
  }

  /// `Изъятие средств`
  String get withdrawMoney {
    return Intl.message(
      'Изъятие средств',
      name: 'withdrawMoney',
      desc: '',
      args: [],
    );
  }

  /// `Введите сумму`
  String get enterAmmount {
    return Intl.message(
      'Введите сумму',
      name: 'enterAmmount',
      desc: '',
      args: [],
    );
  }

  /// `Подтвердить`
  String get confirm {
    return Intl.message(
      'Подтвердить',
      name: 'confirm',
      desc: '',
      args: [],
    );
  }

  /// `Абонплата 1 мес - {fee} сом (Мобильный)`
  String subscriptionFee(String fee) {
    return Intl.message(
      'Абонплата 1 мес - $fee сом (Мобильный)',
      name: 'subscriptionFee',
      desc: '',
      args: [fee],
    );
  }

  /// `\nОсОО Касса плюс #{kassaId}\nМагазин, 72000, с. Панфилова, ул`
  String kassaInfo(String kassaId) {
    return Intl.message(
      '\nОсОО Касса плюс #$kassaId\nМагазин, 72000, с. Панфилова, ул',
      name: 'kassaInfo',
      desc: '',
      args: [kassaId],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<L10n> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'ru'),
      Locale.fromSubtags(languageCode: 'ky'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<L10n> load(Locale locale) => L10n.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}

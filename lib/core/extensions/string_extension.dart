import 'package:base_flutter/core/extensions/datetime_extension.dart';
import 'package:intl/intl.dart';

extension StringExtension on String {
  String get extractDigits {
    if (isEmpty) return '';

    var output = '';
    for (var i = 0; i < length; i++) {
      if (int.tryParse(this[i]) != null) output += this[i];
    }

    return output;
  }

  String get formatAsPhone {
    final digits = extractDigits;
    if (digits.isEmpty) return '';
    final output = <String>[];
    output.add('+${digits.substring(0, 3)}');
    output.add(' (${digits.substring(3, 6)})');
    output.add(' ${digits.substring(6, 9)}-');
    output.add(digits.substring(9, 12));
    output.add(digits.substring(12, digits.length));

    return output.join();
  }

  String get formatddMMMM {
    final day = substring(0, 2);
    final month = substring(3, 5);
    final year = substring(6, 10);
    final timeDate = DateTime.parse('$year$month$day');

    return timeDate.ddMMMM;
  }

  String get formatddMMyyyy {
    final day = substring(0, 2);
    final month = substring(3, 5);
    final year = substring(6, 10);
    final timeDate = DateTime.parse('$year$month$day');

    return timeDate.yyyyMMdd;
  }

  String get formatyMMMM {
    final day = substring(0, 2);
    final month = substring(3, 5);
    final year = substring(6, 10);
    final timeDate = DateTime.parse('$year$month$day');

    return timeDate.yMMMM;
  }

  String get separateNullDouble {
    final parsed = double.parse(this);
    final formatter = NumberFormat.decimalPattern();

    final result = formatter.format(parsed);

    return result;
  }

  int get toInt {
    return int.tryParse(this) ?? 0;
  }

  double get toDouble {
    return double.tryParse(replaceAll(',', '.')) ?? 0.0;
  }

  String get removeExceptDigitsDots =>
      replaceAll(RegExp(r'[^0-9., ]+'), '').replaceAll(' ', '');

  String get removeExceptDigitsDotsBesideMinus =>
      replaceAll(RegExp(r'[^-0-9., ]+'), '').replaceAll(' ', '');

  String get separateByThousands {
    List<String> tempNum;
    if (contains(',')) {
      tempNum = split(',');
    } else {
      tempNum = split('.');
    }
    var text = tempNum[0].extractDigits;
    var length = text.length;
    var subChunks = <String>[];
    var subChunk = '';
    for (var i = length - 1; i >= 0; i--) {
      subChunk = text[i] + subChunk;
      if (subChunk.length % 3 == 0 || i == 0) {
        subChunks.insert(0, subChunk);
        subChunk = '';
      }
    }

    if (contains('.') || contains(',')) {
      tempNum[1] += '00';
      var fractionalText =
          tempNum[1].length > 1 ? tempNum[1].substring(0, 2) : '';
      return '$getSign${subChunks.join(String.fromCharCode(0x00A0))}.$fractionalText';
    } else {
      return '$getSign${subChunks.join(String.fromCharCode(0x00A0))}';
    }
  }

  String get getSign {
    if (contains('-')) {
      return '-';
    }
    return '';
  }
}

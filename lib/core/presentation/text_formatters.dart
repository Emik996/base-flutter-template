import 'package:flutter/services.dart';

class CommaTextInputFormatter extends TextInputFormatter {
  static const String _commaSeparator = ',';
  static const String _spaceSeparator = ' ';

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    // Allow only one comma in the input value
    if (newValue.text.contains(_commaSeparator) &&
        oldValue.text.contains(_commaSeparator) &&
        newValue.text.lastIndexOf(_commaSeparator) !=
            oldValue.text.lastIndexOf(_commaSeparator)) {
      return oldValue;
    }

    // Remove any commas, spaces, and non-digit characters from the input
    String input =
        newValue.text.replaceAll(RegExp('[^\\d$_commaSeparator.]'), '');

    List<String> parts = input.split(',');

    // Format the integer part with comma separators and spaces
    if (parts[0].isNotEmpty) {
      parts[0] = parts[0].replaceAllMapped(
        RegExp(r'\d{1,3}(?=(\d{3})+(?!\d))'),
        (match) => '${match.group(0)}$_spaceSeparator',
      );
    }

    // Limit the decimal part to two digits
    if (parts.length == 2) {
      //
      parts[1] = parts[1].replaceAll(RegExp('[.,]'), '');
      if (parts[1].length > 2) {
        parts[1] = parts[1].substring(0, 2);
      }
    }

    // Join the integer and decimal parts with a decimal point
    input = parts.join(',');

    // Return the formatted value
    return TextEditingValue(
      text: input.replaceAll('.', ','),
      selection: TextSelection.fromPosition(
        TextPosition(offset: input.length),
      ),
    );
  }
}

import 'package:base_flutter/core/data/storage/storage_keys.dart';
import 'package:base_flutter/core/domain/base_cubit_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@injectable
final class SplashCubit extends Cubit<SimpleBaseBlocState> {
  SplashCubit({required SharedPreferences prefs})
      : _prefs = prefs,
        super(const SimpleBaseBlocState.loading());

  final SharedPreferences _prefs;

  String get _token => _prefs.getString(StorageKeys.token) ?? '';

  Future<void> initialize() async {
    await Future.delayed(const Duration(milliseconds: 10));

    if (_token != '') {
      emit(const SimpleBaseBlocState.loaded(null));
    } else {
      emit(const SimpleBaseBlocState.error(null));
    }
  }
}

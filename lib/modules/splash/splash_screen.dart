import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/core/config/di/injection.dart';
import 'package:base_flutter/core/config/router/router.dart';
import 'package:base_flutter/core/domain/base_cubit_state.dart';
import 'package:base_flutter/modules/splash/cubits/splash_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

@RoutePage()
class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final _cubit = di<SplashCubit>()..initialize();

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _cubit,
      child: BlocListener<SplashCubit, SimpleBaseBlocState>(
        bloc: _cubit,
        listener: (context, state) {
          if (state.status == StateStatus.loaded) {
            context.router.pushAndPopUntil(
              const LoginRoute(),
              predicate: (_) => false,
            );
          }
          if (state.status == StateStatus.error) {
            context.router.pushAndPopUntil(
              const LoginRoute(),
              predicate: (_) => false,
            );
          }
        },
        child: const Center(child: CircularProgressIndicator.adaptive()),
      ),
    );
  }
}

import 'package:base_flutter/core/data/network/api_routes.dart' as routes;
import 'package:base_flutter/core/data/network/rest_client.dart';
import 'package:base_flutter/core/domain/base_repo_entity.dart';
import 'package:base_flutter/modules/auhorization/domain/auth_domain_repository.dart';
import 'package:base_flutter/modules/auhorization/features/login/data/models/token_model.dart';
import 'package:base_flutter/modules/auhorization/features/login/domain/usecases/login_usecase.dart';
import 'package:injectable/injectable.dart';

@LazySingleton(as: AuthDomainRepository)
final class AuthRepository implements AuthDomainRepository {
  AuthRepository({
    required RestClient restClient,
  }) : _client = restClient;

  final RestClient _client;

  @override
  Future<BaseResponseEntity<TokenModel>> login({
    required LoginParams? params,
  }) async {
    return await _client.call<TokenModel>(
      RestMethod.post,
      routes.authLogin,
      data: params?.toJson(),
      jsonToModel: TokenModel.fromMap,
    );
  }
}

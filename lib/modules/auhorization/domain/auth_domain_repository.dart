import 'package:base_flutter/core/domain/base_repo_entity.dart';
import 'package:base_flutter/core/domain/base_repository.dart';
import 'package:base_flutter/modules/auhorization/features/login/domain/entities/token_entity.dart';
import 'package:base_flutter/modules/auhorization/features/login/domain/usecases/login_usecase.dart';

abstract class AuthDomainRepository implements BaseRepository {
  Future<BaseResponseEntity<TokenEntity>> login({
    required LoginParams? params,
  });
}

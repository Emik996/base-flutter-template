import 'package:base_flutter/core/extensions/typedefs.dart';
import 'package:base_flutter/modules/auhorization/features/login/domain/entities/token_entity.dart';

final class TokenModel extends TokenEntity {
  final String? maccessToken;

  TokenModel({
    this.maccessToken,
  }) : super(accessToken: maccessToken ?? '');

  factory TokenModel.fromMap(JSON? map) {
    return TokenModel(
      maccessToken: map?['accessToken'] ?? '',
    );
  }
}

base class TokenEntity {
  TokenEntity({
    this.accessToken,
  });

  final String? accessToken;

  TokenEntity copyWith({
    String? accessToken,
  }) {
    return TokenEntity(
      accessToken: accessToken ?? this.accessToken,
    );
  }

  @override
  String toString() => 'TokenEntity(accessToken: $accessToken)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is TokenEntity && other.accessToken == accessToken;
  }

  @override
  int get hashCode => accessToken.hashCode;
}

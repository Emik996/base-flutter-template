import 'package:base_flutter/core/domain/base_params.dart';
import 'package:base_flutter/core/domain/base_repo_entity.dart';
import 'package:base_flutter/core/domain/base_usecase.dart';
import 'package:base_flutter/core/extensions/typedefs.dart';
import 'package:base_flutter/modules/auhorization/domain/auth_domain_repository.dart';
import 'package:base_flutter/modules/auhorization/features/login/domain/entities/token_entity.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class LoginUseCase extends BaseUseCase<TokenEntity, LoginParams> {
  LoginUseCase({required AuthDomainRepository repo}) : _repo = repo;
  final AuthDomainRepository _repo;

  @override
  Future<BaseResponseEntity<TokenEntity>> call(LoginParams params) {
    return _repo.login(params: params);
  }
}

final class LoginParams extends BaseParams {
  LoginParams({
    this.rnm,
    this.password,
  });

  final String? rnm;
  final String? password;

  @override
  JSON toJson() {
    return {
      'rnm': rnm,
      'password': password,
    };
  }
}

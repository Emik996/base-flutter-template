import 'package:auto_route/auto_route.dart';
import 'package:base_flutter/core/config/di/injection.dart';
import 'package:base_flutter/core/config/theme/app_theme.dart';
import 'package:base_flutter/core/data/storage/storage_keys.dart';
import 'package:base_flutter/core/domain/base_cubit_state.dart';
import 'package:base_flutter/core/extensions/context_extension.dart';
import 'package:base_flutter/modules/auhorization/features/login/domain/entities/token_entity.dart';
import 'package:base_flutter/modules/auhorization/features/login/domain/usecases/login_usecase.dart';
import 'package:base_flutter/modules/auhorization/features/login/presentation/cubits/login_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';

@RoutePage()
class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _rnmController = TextEditingController();
  final _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  final _isFullFilled = ValueNotifier(false);
  final _errorText = ValueNotifier<String?>(null);

  final _cubit = di<LoginCubit>();

  void validate(_) {
    if (_rnmController.text.length == 16 &&
        _passwordController.text.isNotEmpty) {
      _isFullFilled.value = true;
    } else {
      _isFullFilled.value = false;
    }
  }

  @override
  void dispose() {
    _rnmController.dispose();
    _passwordController.dispose();
    _isFullFilled.dispose();
    _errorText.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _cubit,
      child: Scaffold(
        body: Container(
          margin: EdgeInsets.fromLTRB(
            16.w,
            MediaQuery.of(context).viewInsets.bottom > 0.0
                ? 0.h
                : AppBar().preferredSize.height.h - 16.h,
            16.w,
            MediaQuery.of(context).viewInsets.bottom > 0.0 ? 0.h : 16.h,
          ),
          padding: EdgeInsets.symmetric(horizontal: 16.w),
          decoration: AppThemeProvider.decors.backgroundDecoration,
          child: Form(
            key: _formKey,
            child: ListView(
              keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
              children: [
                SizedBox(height: 70.h),
                Text(
                  context.lang.input,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 24.h),
                TextField(
                  controller: _rnmController,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  maxLength: 16,
                ),
                SizedBox(height: 24.h),
                ValueListenableBuilder(
                  valueListenable: _errorText,
                  builder: (context, _, __) {
                    return TextField(
                      controller: _passwordController,
                      decoration: InputDecoration(errorText: _errorText.value),
                      onChanged: validate,
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(
                            RegExp(r'^[!a-zA-Z0-9]+')),
                      ],
                      maxLength: 20,
                    );
                  },
                ),
                SizedBox(height: 30.h),
                BlocConsumer<LoginCubit, SimpleBaseBlocState<TokenEntity>>(
                  bloc: _cubit,
                  listener: (context, state) {
                    if (state.status == StateStatus.loaded) {
                      di<SharedPreferences>().setString(
                        StorageKeys.token,
                        state.element?.accessToken ?? '',
                      );
                      di<SharedPreferences>().setString(
                        StorageKeys.rnm,
                        _rnmController.text,
                      );
                    }

                    if (state.status == StateStatus.error) {
                      _errorText.value = state.error?.exception.title;
                    }
                  },
                  builder: (context, state) {
                    return ValueListenableBuilder(
                        valueListenable: _isFullFilled,
                        builder: (context, _, __) {
                          return ElevatedButton(
                            // isDisable: !_isFullFilled.value,
                            // isLoading: state.status == StateStatus.loading,

                            onPressed: () {
                              if (_isFullFilled.value) {
                                _errorText.value = null;
                                _cubit.login(
                                  params: LoginParams(
                                    rnm: _rnmController.text,
                                    password: _passwordController.text,
                                  ),
                                );
                              } else {
                                _formKey.currentState!.validate();
                              }
                            },
                            child: Text(context.lang.comeIn),
                          );
                        });
                  },
                ),
                SizedBox(height: 30.h),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

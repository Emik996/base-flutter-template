import 'package:base_flutter/core/domain/base_cubit_state.dart';
import 'package:base_flutter/modules/auhorization/features/login/domain/entities/token_entity.dart';
import 'package:base_flutter/modules/auhorization/features/login/domain/usecases/login_usecase.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

@injectable
class LoginCubit extends Cubit<SimpleBaseBlocState<TokenEntity>> {
  LoginCubit({required LoginUseCase loginUseCase})
      : _loginUseCase = loginUseCase,
        super(const SimpleBaseBlocState.initial());

  final LoginUseCase _loginUseCase;

  Future<void> login({
    required LoginParams params,
  }) async {
    emit(const SimpleBaseBlocState.loading());

    final result = await _loginUseCase.call(params);

    result.fold((model) {
      emit(SimpleBaseBlocState.loaded(model));
    }, (error) {
      emit(SimpleBaseBlocState.error(error));
    });
  }
}

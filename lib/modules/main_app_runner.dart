import 'dart:async';
import 'dart:io';
import 'package:base_flutter/core/config/di/injection.dart';
import 'package:base_flutter/core/config/flavor_configuration.dart';
import 'package:base_flutter/core/domain/base_app_builder.dart';
import 'package:base_flutter/core/domain/base_app_runner.dart';
import 'package:base_flutter/core/global_observer.dart';
import 'package:device_preview/device_preview.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

class MainAppRunner implements BaseAppRunner {
  @override
  Future<void> preloadData() async {
    HttpOverrides.global = MyHttpOverrides();

    WidgetsFlutterBinding.ensureInitialized();
    // await Firebase.initializeApp(
    //   name: AppFlavors.firebaseName,
    //   options: DefaultFirebaseOptions.currentPlatform,
    // );
    await runZonedGuarded(
      () async {
        await configureDependencies(
          di,
          env: (AppFlavors.flavor as Enum).name,
        );
        await SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitUp,
          DeviceOrientation.portraitDown,
        ]);
        Bloc.observer = di<GlobalObserver>();
        // FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
      },
      (error, stackTrace) {
        if (!kDebugMode) {
          // FirebaseCrashlytics.instance.recordError(error, stackTrace);
        }
      },
    );
  }

  @override
  Future<void> run(BaseAppBuilder builder, Flavor flavor) async {
    AppFlavors.flavor = flavor;
    await preloadData();
    runApp(
      DevicePreview(
        builder: (context) => builder.buildApp(),
        enabled: kDebugMode,
      ),
    );
  }
}

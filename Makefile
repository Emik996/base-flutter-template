get:
	@echo "Geting dependencies"
	@flutter clean && flutter pub get

build: get
	@echo "Running codegeneration"
	@flutter pub run build_runner build --delete-conflicting-outputs
	
watch: get
	@echo "Running codegeneration"
	@flutter pub run build_runner watch --delete-conflicting-outputs

icons: get
	@echo "Running codegeneration"
	@flutter pub run flutter_launcher_icons:main -f flutter_launcher_icons

l10n:
	@echo "Generating localization"
	@flutter pub run intl_utils:generate



run_ios_dev: codegen
	@echo "Running iOS App"
	@open -a simulator
	@flutter run --flavor development --target lib/main_development.dart -a simulator 

run_ios_stage: codegen
	@echo "Running iOS App"
	@open -a simulator
	@flutter run --flavor staging --target lib/main_staging.dart -a simulator

run_ios_prod: codegen
	@echo "Running iOS App"
	@open -a simulator
	@flutter run --flavor production --target lib/main_production.dart -a simulator

apk_dev: 
	@echo "Generating apk..."
	@flutter build apk --release --flavor development --target lib/main_development.dart

apk_prod: 
	@echo "Generating apk..."
	@flutter build apk --release --flavor production --target lib/main_production.dart



mason_init:
	@dart pub global activate mason_cli
	
feature:
	@mason make feature --on-conflict append
	@echo "Running codegeneration"
	@flutter pub run build_runner watch --delete-conflicting-outputs

module:
	@mason make module --on-conflict append
	@echo "Running codegeneration"
	@flutter pub run build_runner watch --delete-conflicting-outputs

arch:
	@mason make architecture --on-conflict overwrite
	@echo "Running codegeneration"
	@flutter pub run build_runner watch --delete-conflicting-outputs

usecase:
	@mason make usecase --on-conflict append
	@echo "Running codegeneration"
	@flutter pub run build_runner watch --delete-conflicting-outputs
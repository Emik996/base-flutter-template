import 'package:base_flutter/core/data/network/network_info.dart';
import 'package:base_flutter/core/data/network/rest_client.dart';
import 'package:base_flutter/modules/auhorization/domain/auth_domain_repository.dart';
import 'package:base_flutter/modules/auhorization/features/login/domain/usecases/login_usecase.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:mockito/annotations.dart';
import 'package:shared_preferences/shared_preferences.dart';

@GenerateMocks(
  [
    /// Data / Data Sources
    Dio,
    NetworkInfo,
    RestClient,
    SharedPreferences,
    Connectivity,

    /// Repositories / Use cases
    AuthDomainRepository,
    LoginUseCase,
  ],
)
void main() {}

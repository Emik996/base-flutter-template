import 'dart:convert';

import 'package:base_flutter/core/domain/base_cubit_state.dart';
import 'package:base_flutter/core/domain/base_error_entity.dart';
import 'package:base_flutter/core/domain/base_repo_entity.dart';
import 'package:base_flutter/modules/auhorization/features/login/data/models/token_model.dart';
import 'package:base_flutter/modules/auhorization/features/login/domain/entities/token_entity.dart';
import 'package:base_flutter/modules/auhorization/features/login/domain/usecases/login_usecase.dart';
import 'package:base_flutter/modules/auhorization/features/login/presentation/cubits/login_cubit.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../../../../../fixtures/fixture_reader.dart';
import '../../../../../../helpers/test_helper.mocks.dart';

void main() {
  late LoginCubit cubit;
  late MockLoginUseCase loginUseCase;

  final testJson = json.decode(fixtureReader('token_model.json'));
  final params = LoginParams(rnm: '', password: '');

  setUp(() {
    loginUseCase = MockLoginUseCase();
    cubit = LoginCubit(loginUseCase: loginUseCase);
  });

  test('Initial state must have StateStatus.initial', () {
    // assert
    expect(cubit.state.status, StateStatus.initial);
  });

  blocTest<LoginCubit, SimpleBaseBlocState<TokenEntity>>(
    'Emits loading -> Runs UseCase -> Emits loaded state with a data',
    build: () => cubit,
    setUp: () {
      when(loginUseCase.call(params)).thenAnswer(
        (_) async => BaseResponseEntity<TokenEntity>(
          model: TokenModel.fromMap(testJson),
        ),
      );
    },
    act: (cubit) => cubit.login(params: params),
    expect: () => [
      const SimpleBaseBlocState<TokenEntity>.loading(),
      SimpleBaseBlocState<TokenEntity>.loaded(TokenModel.fromMap(testJson)),
    ],
    verify: (_) {
      verify(loginUseCase.call(params));
    },
  );

  blocTest<LoginCubit, SimpleBaseBlocState<TokenEntity>>(
    'Emits loading -> Runs UseCase -> Emits error state with a error entity',
    build: () => cubit,
    setUp: () {
      when(loginUseCase.call(params)).thenAnswer(
        (_) async => BaseResponseEntity<TokenEntity>(
          error: CustomErrorEntity(
            statusCode: 500,
            title: 'Internal Server Error',
          ),
        ),
      );
    },
    act: (cubit) => cubit.login(params: params),
    expect: () => [
      const SimpleBaseBlocState<TokenEntity>.loading(),
      SimpleBaseBlocState<TokenEntity>.error(
        CustomErrorEntity(
          statusCode: 500,
          title: 'Internal Server Error',
        ),
      ),
    ],
    verify: (_) {
      verify(loginUseCase.call(params));
    },
  );
}

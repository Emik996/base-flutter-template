import 'dart:convert';
import 'package:base_flutter/core/domain/base_error_entity.dart';
import 'package:base_flutter/core/domain/base_repo_entity.dart';
import 'package:base_flutter/modules/auhorization/features/login/data/models/token_model.dart';
import 'package:base_flutter/modules/auhorization/features/login/domain/entities/token_entity.dart';
import 'package:base_flutter/modules/auhorization/features/login/domain/usecases/login_usecase.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import '../../../../../fixtures/fixture_reader.dart';
import '../../../../../helpers/test_helper.mocks.dart';

void main() {
  late MockAuthDomainRepository mockAuthDomainRepository;
  late LoginUseCase usecase;
  final testJson = json.decode(fixtureReader('token_model.json'));

  setUp(() {
    mockAuthDomainRepository = MockAuthDomainRepository();
    usecase = LoginUseCase(repo: mockAuthDomainRepository);
  });

  test('Should return TokenModel when call auth repository', () async {
    //arrange
    when(
      mockAuthDomainRepository.login(params: anyNamed('params')),
    ).thenAnswer(
      (_) async => BaseResponseEntity<TokenEntity>(
        model: TokenModel.fromMap(testJson),
      ),
    );
    //actual
    final result = await usecase(LoginParams());

    expect(result.model?.accessToken, 'token');
  });

  test('Should return CustomErrorEntity when call auth repository', () async {
    //arrange
    when(
      mockAuthDomainRepository.login(params: anyNamed('params')),
    ).thenAnswer(
      (_) async => BaseResponseEntity<TokenEntity>(
        error: CustomErrorEntity(title: 'Error'),
      ),
    );
    //actual
    final result = await usecase(LoginParams());

    expect(result.error?.exception.title, 'Error');
  });
}

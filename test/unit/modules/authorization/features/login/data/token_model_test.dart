import 'dart:convert';
import 'package:base_flutter/modules/auhorization/features/login/data/models/token_model.dart';
import 'package:base_flutter/modules/auhorization/features/login/domain/entities/token_entity.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../../fixtures/fixture_reader.dart';

void main() {
  final testJson = fixtureReader('token_model.json');
  final tokenModel = TokenModel(maccessToken: 'token');

  test('Should be a subclass of TokenEntity', () async {
    expect(tokenModel, isA<TokenEntity>());
  });

  test('fromJson', () async {
    //arrange
    final Map<String, dynamic> jsonMap = json.decode(testJson);
    // act
    final result = TokenModel.fromMap(jsonMap);
    // assert
    expect(result, tokenModel);
  });
}

import 'dart:convert';
import 'package:base_flutter/core/domain/base_error_entity.dart';
import 'package:base_flutter/core/domain/base_repo_entity.dart';
import 'package:base_flutter/modules/auhorization/data/auth_repository.dart';
import 'package:base_flutter/modules/auhorization/features/login/data/models/token_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import '../../../fixtures/fixture_reader.dart';
import '../../../helpers/test_helper.mocks.dart';

void main() {
  late MockRestClient restClient;
  late AuthRepository authRepository;

  final testJson = json.decode(fixtureReader('token_model.json'));

  setUp(() {
    restClient = MockRestClient();
    authRepository = AuthRepository(restClient: restClient);
  });

  test(
      'Should return TokenModel when call rest client service but error is null',
      () async {
    //arrange
    when(
      restClient.call<TokenModel>(
        any,
        any,
        jsonToModel: TokenModel.fromMap,
      ),
    ).thenAnswer(
      (_) async => BaseResponseEntity<TokenModel>(
        model: TokenModel.fromMap(testJson),
      ),
    );

    //act
    final result = await authRepository.login(params: null);

    //assert
    verify(
      restClient.call<TokenModel>(
        any,
        any,
        jsonToModel: TokenModel.fromMap,
      ),
    );
    expect(result.model, equals(TokenModel.fromMap(testJson)));
    expect(result.error, null);
  });

  test(
      'Should return CustomErrorEntity when call rest client service but model is null',
      () async {
    //arrange
    when(
      restClient.call<TokenModel>(
        any,
        any,
        jsonToModel: TokenModel.fromMap,
      ),
    ).thenAnswer(
      (_) async => BaseResponseEntity<TokenModel>(
        error: CustomErrorEntity(
          statusCode: 500,
          title: 'Internal Server Error',
        ),
      ),
    );

    //act
    final result = await authRepository.login(params: null);

    //assert
    verify(
      restClient.call<TokenModel>(
        any,
        any,
        jsonToModel: TokenModel.fromMap,
      ),
    );
    expect(result.error?.exception.statusCode, 500);
    expect(result.model, null);
  });
}

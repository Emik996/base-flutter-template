import 'package:base_flutter/core/presentation/text_formatters.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test(
      'If value is thousand, it\'s should be formatted as thousands with space',
      () {
    const value = '10000';
    final formatter = CommaTextInputFormatter();

    final editValue = formatter.formatEditUpdate(
      const TextEditingValue(text: value),
      const TextEditingValue(text: value),
    );

    expect(editValue.text, '10 000');
  });
  test(
      'If value has more then 2 symbols after comma, it\'s should be cut to two',
      () {
    const value = '1000,5000';
    final formatter = CommaTextInputFormatter();

    final editValue = formatter.formatEditUpdate(
      const TextEditingValue(text: value),
      const TextEditingValue(text: value),
    );

    expect(editValue.text, '1 000,50');
  });
  test('If value is integer it\'s should be integer w/o comma', () {
    const value = '1000';
    final formatter = CommaTextInputFormatter();

    final editValue = formatter.formatEditUpdate(
      const TextEditingValue(text: value),
      const TextEditingValue(text: value),
    );

    expect(editValue.text, '1 000');
  });
}

import 'package:base_flutter/core/data/network/network_info.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../helpers/test_helper.mocks.dart';

void main() {
  late MockConnectivity mockConnectivity;
  late NetworkInfoImpl networkInfoImpl;

  setUp(() {
    mockConnectivity = MockConnectivity();
    networkInfoImpl = NetworkInfoImpl(connectivity: mockConnectivity);
  });

  test('Should call connection status and get mobile connection', () async {
    // arrange
    when(mockConnectivity.checkConnectivity())
        .thenAnswer((_) async => ConnectivityResult.mobile);
    // act
    await networkInfoImpl.isConnected;
    //assert
    verify(await networkInfoImpl.isConnected);
    expect(await networkInfoImpl.isConnected, true);
  });

  test('Should call connection status and get no connection status', () async {
    // arrange
    when(mockConnectivity.checkConnectivity())
        .thenAnswer((_) async => ConnectivityResult.none);
    // act
    await networkInfoImpl.isConnected;
    //assert
    verify(await networkInfoImpl.isConnected);
    expect(await networkInfoImpl.isConnected, false);
  });
}
